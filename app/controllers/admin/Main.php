<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');
/**
 * @author roman(ukroficer@mail.ru)
 * @copyright 2013
 */
class Main extends Admin_Controller
{
   public function index()
  {

     $this->tpl
          ->js_files('assets/grocery_crud/js/jquery-1.11.2.min')
          ->set('current_section',lang('admin panel'))
          ->set('breadcrumbs',$this->breadcrumbs)
          ->set('menu',$this->menus->render($this->menu, $this->active, null, 'basic'))
          ->set('list_log_play',null)
          ->set('user',$this->user)
          ->set('group_id',$this->group_id)
          ->set('size_dir',$this->get_dir_size())
          ->set('count_films',$this->db->from('movies')->get()->num_rows())
          ->set('count_dirs',$this->get_count_dirs()-1)
          ->set_view('output','admin/index')
          ->build('admin/main');

  }
  function test()
  {
    $this->output->enable_profiler(TRUE);
    //
    $result = $this->db
                   ->select("f.id,f.title,count(lp.id) count_all,from_unixtime(lp.start_time,'%Y-%m-%d')",false)
                   ->from('log_play lp')
                   ->group_by('f.id')
                   ->join('Files f','f.id = lp.id_film')
                   ->where('from_unixtime(lp.start_time,"%Y-%m-%d")'," DATE_SUB(CURRENT_DATE, INTERVAL  0 DAY)",false)
                   ->order_by('count_all','desc')
                   ->get()
                   ->result();
    var_dump($result);

    
    
    
  }
  private function get_count_dirs()
  {
    $this->load->helper('directory');
    $map = directory_map('upls/files');
    return count($map);
  } 
  private function get_dir_size()
  {
    $this->load->helper('number');
    $size =  (int)`du -s /home`*1024;
   
    return  byte_format($size);
  }
}
