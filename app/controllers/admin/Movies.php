<?php
if (!defined('BASEPATH'))
  exit('No direct script access allowed');
class Movies extends Admin_Controller
{

  public $id = null;
  public $dir = 'upls/tmp';
  public $car_id;
  public $i = 1;
  protected $imdb_obj = array();

  protected $rules_movie_download = array(
      'imdb' => array(
          'field' => 'imdb',
          'label' => 'lang:imdb',
          'rules' => 'trim|required|callback__download_imdb'),
      'url'  => array(
          'field' => 'url',
          'label' => 'lang:url',
          'rules' => 'trim|required|encode_php_tags|htmlspecialchars'),
      );


  protected $rules_movie_add = array(
    'imdb' => array(
       'field' => 'imdb',
       'label' => 'lang:imdb',
       'rules' => 'trim|required|callback__imdb'),
    array(
       'field' => 'title',
       'label' => 'lang:title',
       'rules' => 'trim|required|encode_php_tags|htmlspecialchars'),
    array(
       'field' => 'original_title',
       'label' => 'lang:original_title',
       'rules' => 'trim|required|encode_php_tags|htmlspecialchars'),
    array(
       'field' => 'original_language',
       'label' => 'lang:original_language',
       'rules' => 'trim|required|encode_php_tags|htmlspecialchars'),
    array(
       'field' => 'rating',
       'label' => 'lang:rating',
          'rules' => 'trim|required|encode_php_tags|htmlspecialchars'),
    array(
       'field' => 'screenshot',
       'label' => 'lang:screenshot',
       'rules' => 'trim|encode_php_tags|htmlspecialchars'),
    array(
       'field' => 'poster',
       'label' => 'lang:poster',
       'rules' => 'trim|required|encode_php_tags|htmlspecialchars'),
    array(
       'field' => 'file',
       'label' => 'lang:file',
       'rules' => 'trim|required'),
    array(
       'field' => 'overview',
       'label' => 'lang:overview',
       'rules' => 'trim|required|encode_php_tags|htmlspecialchars'),
    array(
       'field' => 'hash',
       'label' => 'lang:hash',
       'rules' => 'trim'),
    array(
       'field' => 'trailer',
       'label' => 'lang:trailer',
       'rules' => 'trim|encode_php_tags|htmlspecialchars'),
    array(
       'field' => 'duration',
       'label' => 'lang:duration',
       'rules' => 'trim|required|encode_php_tags|htmlspecialchars'),
    array(
       'field' => 'quality',
       'label' => 'lang:quality',
       'rules' => 'trim|required|encode_php_tags|htmlspecialchars'),
    array(
       'field' => 'release_date',
       'label' => 'lang:release_date',
       'rules' => 'trim|required|encode_php_tags|htmlspecialchars'),
    array(
       'field' => 'themoviedb_id',
       'label' => 'lang:themoviedb_id',
       'rules' => 'trim|encode_php_tags|htmlspecialchars'),
    array(
       'field' => 'country_id[]',
       'label' => 'lang:country_id',
       'rules' => 'trim|required|encode_php_tags|htmlspecialchars'),
    array(
       'field' => 'actors[]',
       'label' => 'lang:actors',
       'rules' => 'trim|required|encode_php_tags|htmlspecialchars'),
    array(
       'field' => 'director[]',
       'label' => 'lang:directors',
       'rules' => 'trim|required|encode_php_tags|htmlspecialchars'),
    array(
       'field' => 'genre[]',
       'label' => 'lang:genre',
       'rules' => 'trim|required|encode_php_tags|htmlspecialchars'),
    array(
       'field' => 'user_id',
       'label' => 'lang:user_id',
       'rules' => 'trim|encode_php_tags|htmlspecialchars'),
    array(
       'field' => 'status',
       'label' => 'lang:status',
       'rules' => 'trim|required|encode_php_tags|htmlspecialchars'),
    array(
       'field' => 'uri',
       'label' => 'lang:uri',
       'rules' => 'trim'),
    );
  public function __construct()
  {
    parent::__construct();
    if(isset($_SESSION['mydir_wc']))
    {
      $this->dir = $_SESSION['mydir_wc'];
    }

  }
  public function index($action = null, $id = null)
  {

    $this->id = $id;
    $state = $this->crud->getState();
    switch ($state)
    {
        case 'add':
          $this->crud->field_type('user_id','hidden',$this->user->id);
        break;
        case 'edit':
          $this->crud->field_type('user_id','hidden');
        break;
    }
    $this->_get_action($this->crud->getState());
    $this->breadcrumbs['admin/movies'] = lang('movies');
    $this->current_section = lang('published');
    $this->table_bd = 'movies';
    $this->crud
         ->columns('imdb', 'quality','title','date_create','original_title','user','size','release_date','published')
         ->field_type('published', 'true_false');
        //->where('published','1')
         //->field_type('published','hidden','1')
    $this->_generation_list($action,$id);
  }
  public function unpublished($action = null, $id = null)
  {

    $this->id = $id;
    $this->_get_action($this->crud->getState());

    $this->breadcrumbs['admin/movies'] = lang('movies');
    $this->current_section = lang('published');
    $this->table_bd = 'movies';
    $this->crud
         ->unset_add()
         ->columns('imdb','title', 'quality','date_create','size','user','original_title','release_date','publish')
         ->callback_column('publish', array($this, '_callback_publish'))
         ->where('published','0')
         ->field_type('published','hidden','1');

    $this->_generation_list($action,$id);

  } 
  function _generation_list($action = null, $id = null)
{

  $_SESSION['mydir_wc'] = $this->dir;

  $this->crud
      ->unset_read()
      ->callback_column('user', array($this, '_callback_user_published'))
      ->callback_before_delete(array($this, '_delete_movie'))
      ->fields('imdb', 'file','title','original_title','genre','actors','director','original_language','screenshot','poster','rating','overview','trailer','duration','country_id','quality','release_date','themoviedb_id','uri','user_id','published','status')
      ->set_relation('quality','quality','value')
      ->field_type('hash','hidden',null)
      ->field_type('themoviedb_id','hidden',null)
      ->set_field_upload('poster',$this->dir)
      ->set_field_upload('screenshot',$this->dir)
      ->set_rules($this->rules_movie_add)
      ->unset_texteditor('overview')
      ->set_relation_n_n('actors', 'movies_actors', 'persons', 'movie_id', 'person_id', 'name','order_id')
      ->set_relation_n_n('director', 'movies_directors', 'persons', 'movie_id', 'person_id', 'name','order_id')
      ->set_relation_n_n('genre', 'movies_genre', 'genre', 'movie_id', 'genre_id', 'name','order_id')
      ->set_relation_n_n('country_id', 'movies_countries', 'countries', 'movie_id', 'country_id', 'name','order_id')
      ->callback_edit_field('imdb',array($this,'_edit_imdb_callback'))
      ->callback_column('original_title',array($this, '_callback_title'))
      ->callback_field('file', array($this, '_callback_form'))
      ->callback_insert(array($this, '_save'))
      ->callback_update(array($this, '_update'))
      ->set_model('Movies_Grocery_CRUD_Model');
  $this->_example_output();

}
  function downloads($id = null)
  {
    $this->id = $id;
    $this->breadcrumbs['admin/downloads'] = lang('downloads');
    $this->current_section = lang('downloads');
    $this->table_bd = 'downloads';
    $this->crud
         ->unset_delete()
         //->where('published','1')
         ->columns('imdb','date','user_id','published')
         ->add_fields('imdb','url','user_id')
         ->edit_fields('imdb','url','user_id','published')
         ->field_type('published','true_false')
         ->field_type('user_id','hidden',$this->user->id)
         ->unset_texteditor('url')
         ->set_rules($this->rules_movie_download)
         ->display_as('name', lang('name'));
    $this->_example_output();
  }
 
  function _delete_movie($id)
  {
    $file = $this->mod_admin->get_file($id);
    $dir = "upls/files/{$file['imdb']}";
    delete_dir_wc($dir);
  }
  function _callback_title($val,$obj)
  {
    return sprintf("<a data-url='%s/%s/%s'  href='#responsive' class='modal-toggle' data-toggle='modal'  >%s</a>",site_url('upls/files'),$obj->imdb,$obj->file,$val);
  }
  function _callback_user_published($val,$obj)
{
  $result = $this->db->where('id',$obj->user_id)->get('users')->row();
 return sprintf("%s (%s) id %s",$result->name,$result->username,$result->id);
}
  function _get_action($state)
  {

    switch ($state)
    {
      case 'edit':
        $state_info = $this->crud->getStateInfo();
        $this->id = $state_info->primary_key;
        $file = $this->mod_admin->get_file($this->id);
        $this->dir = 'upls/files/'.$file['imdb'];
        @mkdir($this->dir);
        break;
      case 'add':
        $this->dir = 'upls/tmp';
        break;
      case 'list':
      unset($_SESSION['mydir_wc']);
        $this->dir = 'upls/tmp';
      break;
    }
  }
  function _edit_img_callback($image,$id,$obj)
  {
    $movie = $this->mod_admin->get_file($id);
    return $this->tpl
                ->set('image',sprintf('%s/%s/%s',site_url('upls/files'),$movie['hash'],$image))
                ->build('admin/block/image',array('name'=>$obj->name,'value'=>$image),true);
  }
  function _callback_publish($id,$obj)
  {

    return sprintf("<a class='wc_publish' href='%s/%s'>%s</a>",site_url('admin/movies/publish'),$obj->id,'Publish');
  }
  function _callback_form($file = null, $id = null)
  {
    return $this->tpl
                ->set('id', $id)
                ->set('field', form_hidden('file', $file))
                ->set('file', $file)
                ->build('admin/block/uploads', array(), true);
  }
  function upload($id = null)
  {
    $imdb = $this->input->post('imdb');


    if ($id == null)
    {
      $dir = $this->dir;
    } else
    {
      $file = $this->mod_admin->get_file($id);
      $dir = 'upls/files/'.$file['hash'];
    }
    $dir != null ? : show_404();
    try
    {
      $path = "./$dir";
      $config['upload_path'] = $path;
      $config['allowed_types'] = 'mp4';
      $config['encrypt_name'] = true;
      $config['client_name'] = 'QD';
      $config['max_size'] = '999999999';
      $this->load->library('upload', $config);
      if ($this->upload->do_upload('file'))
      {
        $data = $this->upload->data();
        echo json_encode(array(
          'success' => true,
          'data' => $data,
          'file' => $data['file_name'],
          'field' => form_hidden('file', $data['file_name']),
          'fields'=>json_encode($this->_get_data_movie($imdb,$dir)),
          ));
      } else
      {
        echo json_encode(array(
          'success' => false,
          'message' => strip_tags($this->upload->display_errors()),
          'success' => false));
        
      }
    }
    catch (exception $e)
    {
      echo json_encode(array(
        'success' => false,
        'message' => strip_tags($e->getMessage()),
        'success' => false));
    }
 
  }
  function _save($data,$id = null)
  {
    $data['uri'] = $this->mod_admin->get_is_uri($data['title'], null,'movies', $id);
    $data['hash'] = $this->mod_admin->hash_uri($data['title'], md5($data['uri'] . 'Hash'), 'movies', $id, 'hash');
    $this->mod_admin->save_movie($data,$this->dir);
    $this->cache->memcached->groupdelete('movie');
    return true;
  }
  function _update($data,$id = null)
  {

    $data['uri'] = $this->mod_admin->get_is_uri($data['title'], $data['uri'],'movies', $id);
    $data['hash'] = $this->mod_admin->hash_uri($data['title'], md5($data['uri'].time(). 'Hash'), 'movies', $id, 'hash');
    $this->mod_admin->update_movie($data,$this->dir,$id);
    $this->cache->memcached->groupdelete('movie');
  return true;
  }

  function validation($id = null)
  {
    $imdb = $this->input->post('imdb');
    $this->load->library('moviedb');
    if($this->moviedb->movieortv($imdb)!='movie')
   {
    echo json_encode(array(
        'success' => false,
        'message' => 'This imdb not is movie',
        'success' => false));
    exit();
   }
    if(empty($imdb))
    {
      echo json_encode(array(
                       'success' => false,
                       'message' => 'Field imdb empty',
                       'success' => false));
      exit();
    }
    $this->id = $id;
    
    if(!$this->_imdb($imdb))
    {
      echo json_encode(array(
          'success' => false,
          'message' => $this->form_validation->get_error_messages('_imdb'),
          ));
      exit();
    }
      echo json_encode(array(
           'success' => true,
      ));
  }
  function publish($id)
  {
    $this->load->library('form_validation');
    $this->id = $id;
    $this->form_validation->validation_data = $this->mod_admin->get_movie($id);

    $this->form_validation->set_rules($this->rules_movie_add);
    if ($this->form_validation->run())
    {   
      $this->db->where('id',$id)->set('published','1')->update('movies');
      $data = $this->mod_admin->get_file($id);
      echo json_encode(['success' => true,'url'=> sprintf('%s/%s/%s',site_url('upls/files'),$data['hash'],$data['file'])]);
    }
    else
    {
      echo json_encode(['success' => false,'fields'=>$this->form_validation->error_array()]);

    }
 
  }

  function _download_imdb($imdb)
  {
    $state = $this->crud->getStateInfo();
    $id = null;
    if(isset($state->primary_key))
    {
      $id = $state->primary_key;
    }

    $result = $this->db->where('imdb',$imdb)->where('id!=',$id)->get('downloads')->row();
    $movies = $this->db->where('imdb',$imdb)->where('id!=',$id)->get('downloads')->row();


    if($result!=null)
    {
      return false;
    }


    if(!$this->_imdb($imdb))
    {
      $this->form_validation->set_message('_imdb', 'This Imdb not valid');
      return false;
    }




  }
  function test($imdb)
  {


    $data= file_get_contents("https://api.themoviedb.org/3/find/{$imdb}?external_source=imdb_id&api_key=37412d1b50991ad58b707aa48b992dac&language=de");
    echo '<pre>';
     print_r(json_decode($data));
    echo '</pre>';

    $data= file_get_contents("https://api.themoviedb.org/3/tv/1418/season/2/episode/2/external_ids?api_key=37412d1b50991ad58b707aa48b992dac&language=de");
    echo '<pre>';
     print_r(json_decode($data));
    echo '</pre>';
  }

  function _edit_imdb_callback($value)
  {
    return "<input type='text' name='imdb' value='$value' readonly>";
  }


}
