<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * @author roman(ukroficer@mail.ru)
 * @copyright 2013
 */

class Cron extends MY_Controller
{
   public function __construct()
   {
     parent::__construct();
     $this->load->helper('directory');
     $this->load->library('moviedb');
     $this->load->model('mod_admin');
   }
   function index()
   {

       //$this->db->set('time',null)->insert('cron');
       $min_time = 60;
       $max_time = 60*60*128;
       $users = $this->db
                     ->where('cron','1')
                     //->where('id',29)
                     ->order_by('id','random')
                     ->get('users')
                     ->result();
       foreach ($users as $user)
       {
           $dir = '../movies/'.$user->id;
           @mkdir($dir);
           $dir_array = directory_map($dir);


           foreach (array_slice ((array)$dir_array,0,50) as $k=>$v) {

               $patch_file = $dir . '/' . $v;

               $ext = pathinfo($v, PATHINFO_EXTENSION);
               $filename = pathinfo($v, PATHINFO_FILENAME);
               $filetime = filectime($patch_file);
               $long = time() - $filetime;
               if ($ext == 'mp4' && $long > $min_time && $long < $max_time) {
                   $_POST = array();

                   $result = explode('-', $filename);
                   $imdb = $_POST['imdb'] = $result['0'];
                   $mv= $this->db->where('imdb',$imdb)->get('movies')->row();
                   if($mv)
                   {
                       if($mv->size == 0)
                           var_dump('empty file - '.$imdb);
                   }
                   if (!$this->_imdb($imdb)) {

                       continue;
                   }


                   if($this->moviedb->movieortv($imdb) == 'movie')
                   {

                       $movie = $this->_get_data_movie($imdb, $dir, true);
                       foreach ($movie as $ke => $va) {
                           if (is_array($va)) {
                               foreach ($va as $key => $val) {
                                   $_POST[$ke][] = $key;
                               }

                           } else {
                               $_POST[$ke] = $va;
                           }
                       }

                       $_POST['quality'] = isset($result['1']) ? $result['1'] : null;
                       $data['uri'] = $this->mod_admin->get_is_uri($_POST['title'], null, 'movies', null);
                       $data['hash'] =        $this->mod_admin->hash_uri($_POST['title'], md5($data['uri'] . time() . 'Hash'), 'movies', null, 'hash');
                       $_POST['file'] = $v;
                       $_POST['status'] = '1';
                       $_POST['published'] = '0';
                       $_POST['user_id'] = $user->id;

                       $this->mod_admin->save_movie($data,$dir);
                   $handle = fopen('pub.log', "a+");
                   fwrite($handle,date('j.m.Y G:i:s').' - '.$patch_file."\r\n");
                   fclose($handle);


                   }



               } elseif (($long > $max_time)) {


                   $handle = fopen('clean.log', "a+");
                   fwrite($handle,date('j.m.Y G:i:s').' - '.$patch_file.'-'.date('Y-m-d H:i:s',filemtime($patch_file)).'-'.date('Y-m-d H:i:s',filectime($patch_file))."\r\n");
                   fclose($handle);

               }

           }
       }




   }
    function downloads($id = null)
    {

       return false;
       $movies =  $this->db
                       ->where('published','0')
                      // ->where('user_id',14)
                       ->limit(3)
                       ->order_by('id','random')
                       ->get('downloads')
                       ->result();


       foreach (array_slice ((array)$movies,0,2) as $k=>$v) {
           $result = $this->db->where('imdb', $v->imdb)->from('movies')->get()->row();
           $this->db->where('id',$v->id)->set('published','1')->update('downloads');
           if ($result == null)
           {
               $dir = '../movies/'.$v->user_id;
               @mkdir($dir);
               $handle = fopen('down.log', "a+");
               fwrite($handle,date('j.m.Y G:i:s').' - '.$v->imdb.'-'.$dir."\r\n");
               fclose($handle);

               $this->db->set('status','0')->where('id',$v->id)->update('downloads');
               $this->download($v->url, "{$dir}/{$v->imdb}.mp4");

           }

       }
    }
    private  function download($url, $path){

        ignore_user_abort(true);
        set_time_limit(0);
        $command = "wget --tries=3 -O '".$path."' '".$url."' 2>&1";

        $output = shell_exec($command);

        if($output != null){
            if(preg_match('|«'.$path.'» saved|si', $output)){
                return true;
            }
        }

        return false;
    }
    function clean()
    {
        $dir = 'upls/tmp';
        $dir_array = directory_map($dir);
        $limit_time = 60 * 60 * 24;
        foreach ($dir_array as $k => $v) {
            $patch_file = $dir . '/' . $v;
            $time = filectime($patch_file);
            if (time() - $time > $limit_time) {
                @unlink($patch_file);
            }
        }
    }
    function test()
    {

    }
}

