<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
/**
 * @author roman(ukroficer@mail.ru)
 * @copyright 2013
 */

class Main extends Public_Controller
{
    public $offset = 50;
    public $_route = false;
    public $ss = 50;
    public function index($page = 0)
    {

      $this->tpl
           ->set_view('content','index')
           ->build();

    }

    public function page($uri)
    {
        $page = $this->db->where('uri',$uri)->from('pages')->get()->row();
        $page != null ? : show_404();
        $this->tpl
            ->set_breadcrumbs(array(site_url() => 'Main'))
            ->set_breadcrumbs(array(site_url('page/' . $page->uri) => $page->name))
            ->set('page', $page)
            ->set_view('content', 'page')
            ->build();
    }
    public function hash($hash)
    {
        $movie = $this->db->where('hash',$hash)->from('movies')->get()->row();

        $movie != null ? : show_404();
        $this->tpl
             ->set('movie', $movie)
             ->set('video',site_url('upls/files/'.$movie->imdb.'/'.$movie->file))
            // ->set('video','http://videoboxer.co/upls/files/2ad3c1fb/game-of-thrones-s06e07-the-broken-man.mp4')

             ->set_view('content', 'video')
             ->build();
    }
    function similar($id,$token = null)
    {
        if($token!=='klm404')
        {
            return false;
        }
        $this->load->library('moviedb');
        $similar = $this->moviedb->get_similar($id);
        $this->db->where('themoviedb_id',$id)->delete('similar');
        foreach ($similar as $k=>$v)
        {
            $this->db
                 ->set('themoviedb_id',$id)
                 ->set('similar_themoviedb_id',$v->id)
                 ->insert('similar');
        }
    }
    function diff()
    {
        $this->load->helper('directory');
        $result = $this->db->from('movies')->get()->result();
        foreach ($result as $k=>$v)
        {
            $movies[] = $v->imdb;
        }
        $result_dir = $dir_array = directory_map('upls/files');
        $dir = array();
        foreach ($result_dir as $k=>$v)
        {
            $dir[] = trim($k,'/');
        }
        $data = array();
        foreach ($dir as $k=>$v)
        {

            // if(!in_array($v,$movies))
           // {
               echo $v.'</br>';
            //}
        }
        var_dump(count($dir),count($movies));

        
    }

}

