<?php
function delete_dir_wc($dir)
{
  $CI = &get_instance();
  $CI->load->helper('directory');
  $map = directory_map($dir);
  if (is_array($map))
  {
    foreach ($map as $k => $v)
    {
      if (is_array($v))
      {
        delete_dir_wc($dir . DIRECTORY_SEPARATOR . $k);
      } else
      {
        @unlink($dir . DIRECTORY_SEPARATOR . $v);
      }
    }
    @rmdir($dir);
  }
  return true;
}
if (!function_exists('mb_ucfirst') && extension_loaded('mbstring'))
{
  /**
   * mb_ucfirst - преобразует первый символ в верхний регистр
   * @param string $str - строка
   * @param string $encoding - кодировка, по-умолчанию UTF-8
   * @return string
   */
  function mb_ucfirst($str, $encoding = 'UTF-8')
  {
    $fc = mb_strtoupper(mb_substr($str, 0, 1));
    return $fc . mb_substr($str, 1);
  }
}

if (!function_exists('dirsize_wc'))
{
  /**
   * mb_ucfirst - преобразует первый символ в верхний регистр
   * @param string $str - строка
   * @param string $encoding - кодировка, по-умолчанию UTF-8
   * @return string
   */
   function dirsize_wc($dir) {
    if(is_file($dir)) return array('size'=>filesize($dir),'howmany'=>0);
    if($dh=opendir($dir)) {
        $size=0;
        $n = 0;
        while(($file=readdir($dh))!==false) {
            if($file=='.' || $file=='..') continue;
            $n++;
            $data = dirsize_wc($dir.'/'.$file);
            $size += $data['size'];
            $n += $data['howmany'];
        }
        closedir($dh);
        return array('size'=>$size,'howmany'=>$n);
    } 
    return array('size'=>0,'howmany'=>0);
  }
}
?>