<?php 
class Ya
{
    public $token_array = null;
    public $auth_params  = array(
                    'grant_type' => 'password',
                    'username' => 'boxan1984',
                    'password' => 'ICRHmzyVmU',
                    'client_id' => '55f15da347ed46d7825c76cc5190c7cd',
                    'client_secret' => '9a770851b30949d1adced641970513a4');
    public $url_auth = "https://oauth.yandex.ru/token";
    public $public_key = array();             
  	public function __construct()
	{
	      $this->ci = &get_instance();
          $this->ci->load->library('curl'); 
          $this->ci->curl->post($this->url_auth,$this->auth_params);
          $this->token_array = $this->ci->curl->exec();
          $this->ci->curl->setHeader('Accept','application/json'); 
          $this->ci->curl->setHeader('Authorization','OAuth '. $this->token_array->access_token); 
	} 
    function get_token()
    {
        return $this->token_array;
    }
    function publish_file($path)
    {
	
		
        $this->ci->curl->put("https://cloud-api.yandex.net:443/v1/disk/resources/publish?path=".$path); 
        
        $data =  $this->ci->curl->exec();
        if(isset($data->error))
        {
            return false;
        }
        return $data;
    }
    function get_file($path,$publish = true)
    {
        if($publish)
        {
           $result_publish =  $this->publish_file($path);
           if(!$result_publish)
           {
            return $result_publish;
           }
        }
        $data = $this->ci->curl->get("https://cloud-api.yandex.net:443/v1/disk/resources",array('path'=>$path)); 
        
        
        if(isset($data->error))
        {
            return false;
        }
        return $data;
    }
    function get_public_files($path)
    {
          $file = $this->get_file($path,true);
           if(!$file)
          {
            return $file;
          }
          $this->ci->curl->get("https://cloud-api.yandex.net:443/v1/disk/resources/public",array('public_key'=>$file->public_key)); 
          $data = $this->ci->curl->exec(); 
          if(isset($data->error))
          {
             return false;
          }
          return $data;
    }
     function get_public_link($path)
    {
          $file = $this->get_file($path,true);
           if(!$file)
          {
            return $file;
          }
          
         $data = $this->ci->curl->get("https://cloud-api.yandex.net:443/v1/disk/public/resources/download?public_key=".$file->public_url); 
 
      
          if(isset($data->error))
          {
             return false;
          }
          return $data;
    }
    function move_file($file,$url)
    {
         $url = urlencode(prep_url($url));
         $file = urlencode($file);
         $this->ci->curl->post("https://cloud-api.yandex.net:443/v1/disk/resources/upload?path={$file}&url={$url}&disable_redirects=true"); 
         $data =  $this->ci->curl->exec();
         if(isset($data->error))
         {
            return false;
         }
         return $data;
    }
    function deletefile($path)
    {
          $path = urlencode($path);
          $this->ci->curl->delete("https://cloud-api.yandex.net:443/v1/disk/resources?path={$path}&permanently=true"); 
          $data =  $this->ci->curl->exec();
         if(isset($data->error))
         {
            return false;
         }
         return $data;
    }
}
