<?php
class Grid{
	
    public $realWidth = 720;
    public $realHeight = 360;
    
    public $gridWidth = 4;
    public $gridHeight = 3;
    
    public $cellWidth;
    public $cellHeight;
    
    private $image;
    
    public function __construct(){
        
        
        $this->image = imagecreatetruecolor($this->realWidth, $this->realHeight);
        
        $transparent = imagecolorallocate($this->image, 255, 255, 255);
		
		imagefill($this->image, 0, 0, $transparent);
		imagecolortransparent($this->image, $transparent);
    }
    
    public function showGrid(){
		$black = imagecolorallocate($this->image, 0, 0, 0);
		
		imagesetthickness($this->image, 3);
		
		$this->cellWidth = ($this->realWidth - 1) / $this->gridWidth;   // note: -1 to avoid writting
	    
    
    	$this->cellHeight = ($this->realHeight - 1) / $this->gridHeight; // a pixel outside the image
		
		for($x = 0; ($x <= $this->gridWidth); $x++){
			for($y = 0; ($y <= $this->gridHeight); $y++){
				imageline($this->image, ($x * $this->cellWidth), 0, ($x * $this->cellWidth), $this->realHeight, $black);
				imageline($this->image, 0, ($y * $this->cellHeight), $this->realWidth, ($y * $this->cellHeight), $black);
		
        	}
		}
	}
	
	public function putImage($img, $realSizeW, $realSizeH, $origin_width, $origin_height, $realPosX, $realPosY){
		if(is_string($img)){
			$pathinfo = pathinfo($img);
			
			if($pathinfo['extension'] == 'jpg' || $pathinfo['extension'] == 'jpeg'){
				$img = imagecreatefromjpeg($img);
			}elseif($pathinfo['extension'] == 'png'){
				$img = imagecreatefrompng($img);
			}
		}
		
		$img = $this->resize($img, $realSizeW, $realSizeH, $origin_width, $origin_height);
		
		imagecopyresampled($this->image, $img, $realPosX, $realPosY, 0, 0, $realSizeW, $realSizeH, imagesx($img), imagesy($img));
	}
    
    public function resize($img, $targetWidth, $targetHeight, $origin_width, $origin_height){
		$src_aspect = $origin_width / $origin_height; //��������� ������ � ������ ���������
		$thumb_aspect = $targetWidth / $targetHeight; //��������� ������ � ������ ��������� �����
		
		//����� ������� (������������� ������)
		if($src_aspect < $thumb_aspect){ 		
			$scale = $targetWidth / $origin_width;
			$new_size = array($targetWidth, $targetWidth / $src_aspect);
			
			//���� ���������� �� ������ �� ���� �������� �� ������ ������� ����� �������
			$src_pos = array(0, ($origin_height * $scale - $targetHeight) / $scale / 2);
		}else if($src_aspect > $thumb_aspect){
			//������� ������� (������������� ������)
			$scale = $targetHeight / $origin_height;
			$new_size = array($targetHeight * $src_aspect, $targetHeight);
			
			//���� ���������� �� ������ �� ���� �������� �� ������ ������� ����� �������
			$src_pos = array(($origin_width * $scale - $targetWidth) / $scale / 2, 0);
		}else{
			//������
			$new_size = array($targetWidth, $targetHeight);
			$src_pos = array(0,0);
		}
		
		$new_size[0] = max($new_size[0], 1);
		$new_size[1] = max($new_size[1], 1);
		
		$img_o = imagecreatetruecolor($targetWidth, $targetHeight);
		
		imagecopyresampled($img_o, $img, 0, 0, $src_pos[0], $src_pos[1], $new_size[0], $new_size[1], $origin_width, $origin_height);
		
		return $img_o;
	}
    
    public function __destruct(){
        imagedestroy($this->image);
    }
    
    public function save($file = false){
		if(!$file){
			header("Content-type: image/jpeg");
			imagejpeg($this->image);
		}else{
			return imagejpeg($this->image, $file.".jpeg");
		}
    }
  

}
