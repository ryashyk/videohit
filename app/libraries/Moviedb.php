<?php
 class Moviedb
 {
     protected $base_url = 'https://api.themoviedb.org/3/';
     public $api_key = '37412d1b50991ad58b707aa48b992dac';
     public $token = '19a260d0-825e-40ad-a4aa-e7a50a65cfa4';
     public $omdbapi_url = 'http://www.omdbapi.com/?';
     public $omdbapi_apikey = "e0e57c7";
     public $url_myapifilms = 'http://api.myapifilms.com/imdb/idIMDB?';
     protected $language = 'de';
     protected $path_poster = "http://image.tmdb.org/t/p/original";

     function get_similar($id)
     {
       $data =  $this->query($this->base_url,"movie/{$id}/similar?api_key={$this->api_key}");
       if(isset($data->results))
       {
          return  $data->results;
       }
         else
             return false;
     }
     function get_genres()
     {
       return $this->query($this->base_url,"genre/movie/list?api_key={$this->api_key}&language={$this->language}");
     }
     function movieortv($imdb)
     {

         $movie = $this->query($this->omdbapi_url,"i={$imdb}");
         if(isset($movie->Type))
         {
          switch ($movie->Type) {
              case "movie":
              return 'movie';
              break;
              case "TV Episode":
              return 'tv_episode';
              break;
              case "TV Series":
              return 'tv_series';
              break;
              default:
                  return false;
             }
          }
         else
         {
             return false;//
         }
     }
     function get_screenshot($id,$num = 0)
     {
         if($id == null)
         {
             return false;
         }
         $data= $this->query($this->base_url,"movie/{$id}/images?api_key={$this->api_key}");
         if(isset($data->backdrops[$num]->file_path))
         {
             return $data->backdrops[$num]->file_path;
         }
         else{
             if(isset($data->backdrops[0]->file_path))
             {
                 return $data->backdrops[0]->file_path;
             }
         }

         return false;
     }
     function save_poster($name,$poster,$dir)
     {
         $file = $this->path_poster.$poster;
         $newfile = "{$dir}/{$name}";

         if (@!copy($file, $newfile)) {
             return null;
         }
         return $newfile;
     }
     function get_episode_info()
     {

     }
     function get_tv_imdb($id)
     {
         $data = $this->query($this->base_url,"tv/{$id}/external_ids?external_source=imdb_id&api_key={$this->api_key}&language={$this->language}");

         if(isset($data->imdb_id))
         {
             return $data->imdb_id;
         }
         return false;
     }
     function get_season_info($id,$season)
     {
         $season = $this->query($this->base_url,"tv/{$id}/season/{$season}?external_source=imdb_id&api_key={$this->api_key}&language={$this->language}");
         return $season;
     }
     function get_tv_info($id)
     {
         $tv = $this->query($this->base_url,"tv/{$id}?external_source=imdb_id&api_key={$this->api_key}&language={$this->language}");
         $tv->persons = $this->query($this->base_url,"tv/{$id}/credits?external_source=imdb_id&api_key={$this->api_key}&language={$this->language}");
         $tv->imdb = $this->get_tv_imdb($id);

         return $tv;
     }
     function get_tv($imdb = null)
     {
         if($imdb == null)
         {
             return false;
         }

         $data= $this->query($this->base_url,"find/{$imdb}?external_source=imdb_id&api_key={$this->api_key}&language={$this->language}");
         $result = array('tv'=>(object)array(),'season'=>(object)array(),'episode'=>(object)array());
         if(isset($data->tv_results[0]->id))
         {
             $result['tv'] = $this->get_tv_info($data->tv_results[0]->id);
         }
         elseif($data->tv_episode_results[0]){
             $result['tv'] = $this->get_tv_info($data->tv_episode_results[0]->show_id);
             //$result['season'] = $this->get_tv_info($data->tv_episode_results[0]->show_id,$data->tv_episode_results[0]->season_number);
            // $result['episode'] = $data->tv_episode_results[0];
         }
         if(empty($result))
         {
             return false;
         }
         else
         {
             return $result;
         }


     }
     function get_movie($imdb = null)
     {
       if($imdb == null)
       {
           return false;
       }
       $data= $this->query($this->base_url,"find/{$imdb}?external_source=imdb_id&api_key={$this->api_key}&language={$this->language}");
      
       if(isset($data->movie_results['0']))
       {
           return $data->movie_results['0'];
       }

       return false;
     }
     function get_credits($id)
     {
        if($id == null)
       {
         return false;
       }
       $data= $this->query($this->base_url,"movie/{$id}/credits?api_key={$this->api_key}&language={$this->language}");

       return $data;
     }
     function get_person($id)
     {
         if($id == null)
         {
             return false;
         }
         $data = $this->query($this->base_url,"person/{$id}?external_source=imdb_id&api_key={$this->api_key}&language={$this->language}");

         return $data;
     }
     function get_videos($id)
     {
         if($id == null)
         {
             return false;
         }
         $data = $this->query($this->base_url,"movie/{$id}/videos?external_source=imdb_id&api_key={$this->api_key}&language={$this->language}");
         if(isset($data->results))
         {
            foreach ($data->results as $k=>$v)
            {
                if($v->site == 'YouTube')
                {
                    return $v->key;
                }
            }
         }
         return null;
     }
     function query($base_url,$sUrl) {
         $req = curl_init();

         curl_setopt($req, CURLOPT_URL, $base_url.$sUrl);
         curl_setopt($req, CURLOPT_RETURNTRANSFER, 1);
         $response = json_decode(curl_exec($req));
         curl_close($req);
         return $response;
     }
 }