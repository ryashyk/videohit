<?php
Class Proxy{
	
	public $_cookie = '';
	public $_proxy = '';
	public $_agent = '';
	
	public $_timeout = 3;
	
	public $_debug = true;
		public function __construct(){
		$proxy = array(
			'50.207.187.254:8080',
			'91.142.84.182:3128',
			'83.169.211.50:3128',
			'62.109.5.30:81',
			'85.143.164.100:81',
			'46.173.188.103:3128',
			'37.46.129.238:8080'
		);
		
		$this->_proxy = $proxy[array_rand($proxy)];
		
		return $this;
	}
	public function curl($url, $json = false){
		$ch = curl_init($url);
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_TIMEOUT, $this->_timeout);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		
		if($this->_agent != null){
			curl_setopt($ch, CURLOPT_USERAGENT, $this->_agent);
		}
		
		if($this->_proxy != null){
			//устанавливаем прокси
			curl_setopt($ch, CURLOPT_PROXY, $this->_proxy);
		}
		
		if($this->_cookie != null){
			curl_setopt($ch, CURLOPT_COOKIE, $this->_cookie);
		}
		
		curl_setopt($ch, CURLOPT_HEADER, false);
		
		if($this->_debug){
			curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		}
		
		$ret = curl_exec($ch);
		
		if($this->_debug){
            echo $ret;
			echo "<br>";
			
			$info = curl_getinfo($ch);
			
			echo"<pre>";
			print_r($info);
			echo"</pre>";
			
			$info = curl_getinfo($ch, CURLINFO_HEADER_OUT);
			
			echo"<pre>";
			print_r($info);
			echo"</pre>";
	}
		
		if($ret != null){
			$info = curl_getinfo($ch);
			curl_close($ch); // close cURL handler
			
			if(!isset($info['http_code']) || $info['http_code'] !== 200){
				return false;
			}
		}else{
			curl_close($ch);
			
			return false;
		}
		
		if($json){
			return json_decode($ret);
		}
		
		return $ret;
	}
	
	public function set_user_agent(){
		$agents = array(
			'Mozilla/5.0 (Windows; U; Windows NT 5.1; ru; rv:1.9.0.1) Gecko/2008070208'
		);
		
		$this->_agent = $agents[array_rand($agents)];
		
		return $this;
	}
	

}

//$test = new test;
//$test->set_proxy();
//
//$data = $test->curl('http://www.omdbapi.com/?i=tt2488496&plot=full&r=json&apikey=740e5f29', true);
//
//if($data){
//	echo"<pre>";
//	print_r($data);
//	echo"</pre>";
//}
