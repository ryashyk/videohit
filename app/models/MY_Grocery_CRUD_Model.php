<?php

class MY_Grocery_CRUD_Model extends Grocery_CRUD_Model
{
    public $unset_field = array('nid','user');

    function get_total_results()
    {
      
        
        //set_relation_n_n special queries. We prefer sub queries from a simple join for the relation_n_n as it is faster and more stable on big tables.
        if(!empty($this->relation_n_n))
        {
            $select = "{$this->table_name}.*";
            $select = $this->relation_n_n_queries($select);

            $this->db->select($select,false);
        }

        return $this->db->get($this->table_name)->num_rows();
    }

    function get_list()
    {
        if($this->table_name === null)
            return false;

        $select = "`{$this->table_name}`.*";

        //set_relation special queries
        if(!empty($this->relation))
        {
            foreach($this->relation as $relation)
            {
                list($field_name , $related_table , $related_field_title) = $relation;
                $unique_join_name = $this->_unique_join_name($field_name);
                $unique_field_name = $this->_unique_field_name($field_name);

                if(strstr($related_field_title,'{'))
                {
                    $related_field_title = str_replace(" ","&nbsp;",$related_field_title);
                    $select .= ", CONCAT('".str_replace(array('{','}'),array("',COALESCE({$unique_join_name}.",", ''),'"),str_replace("'","\\'",$related_field_title))."') as $unique_field_name";
                }
                else
                {
                    $select .= ", $unique_join_name.$related_field_title AS $unique_field_name";
                }

                if($this->field_exists($related_field_title))
                    $select .= ", `{$this->table_name}`.$related_field_title AS '{$this->table_name}.$related_field_title'";
            }
        }

        //set_relation_n_n special queries. We prefer sub queries from a simple join for the relation_n_n as it is faster and more stable on big tables.
        if(!empty($this->relation_n_n))
        {
            $select = $this->relation_n_n_queries($select);
        }

        $this->db->select($select, false);

        $results = $this->db->get($this->table_name)->result();

        return $results;
    }

    function where($key, $value = NULL, $escape = TRUE)
    {
        if (!in_array($key, $this->unset_field)) ;
        {
            $this->db->where($key, $value, $escape);
        }

    }

    function or_where($key, $value = NULL, $escape = TRUE)
    {
        if (!in_array($key, $this->unset_field)) ;
        {
            $this->db->or_where($key, $value, $escape);
        }

    }

    function like($key, $value = NULL, $escape = TRUE)
    {
        $this->firephp->log($key);
        if (!in_array($key, $this->unset_field)) ;
        {
            $this->db->like($key, $value, $escape);
        }

    }

    function or_like($key, $match = '', $side = 'both')
    {

        if (!in_array($key, $this->unset_field)) ;
        {
            
            $this->db->or_like($key, $match, $side);
        }
    }


}