<?php
class Mod_admin extends MY_Model
{
    
    public $cache_array = array();
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function get_movie($id)
    {
        $data = $this->get_file($id);
        $actors =  $this->db
                        ->from('persons p')
                        ->join('movies_actors ma','ma.person_id=p.id')
                        ->where('movie_id',$id)
                        ->order_by('ma.order_id','asc')
                        ->get()
                        ->result();
        $director =  $this->db
                        ->from('persons p')
                        ->join('movies_directors md','md.person_id=p.id')
                        ->where('movie_id',$id)
                        ->order_by('md.order_id','asc')
                        ->get()
                        ->result();
        $countries = $this->db
                         ->from('countries c')
                         ->join('movies_countries mc','mc.country_id=c.id')
                         ->where('movie_id',$id)
                         ->order_by('mc.order_id','asc')
                         ->get()
                         ->result();
        $genre = $this->db
                         ->from('genre g')
                         ->join('movies_genre mg','mg.genre_id=g.id')
                         ->where('movie_id',$id)
                         ->order_by('mg.order_id','asc')
                         ->get()
                         ->result();

        $data['country_id'] = $this->get_list($countries,'name',false,'id',false);
        $data['actors']     = $this->get_list($actors,'name',false,'id',false);
        $data['director']    = $this->get_list($director,'name',false,'id',false);
        $data['genre']     = $this->get_list($genre,'name',false,'id',false);

        return $data;
    }
    public function get_file($id)
    {
        return $this->db
            //->select('name')
            ->where('id',$id)
            ->from('movies')
            ->get()
            ->row_array();
    }
    function update_movie($data,$dir,$id = null)
    {
        $name_file = $this->input->post('file');
        $ext = pathinfo($data['file'], PATHINFO_EXTENSION);
        $namefile = 'video';//url_title(mb_strtolower(convert_accented_characters($data['title'])));
        $data['file'] = $namefile.'.'.$ext;
        $data['size'] = filesize($dir.'/'.$name_file);


        if (empty($this->input->post('screenshot'))) {

            $movie = new ffmpeg_movie($dir.'/'.$name_file);
            $s = (int)($movie->getFrameCount()*2.5/100);
            $image = $movie->getFrame($s);
            $show_img = $image->toGDImage();
            $_POST['screenshot'] = sprintf('screenshot_%s.jpg',$this->input->post('imdb'));
            imagejpeg($show_img,$dir.'/'.$this->input->post('screenshot'));

        }

        $this->db
             ->where('id',$id)
             ->set('published',$this->input->post('published'))
             ->set('imdb',$this->input->post('imdb'))
             ->set('file',$this->input->post('file'))
             ->set('title',$this->input->post('title'))
             ->set('original_language',$this->input->post('original_language'))
             ->set('original_title',$this->input->post('original_title'))
             ->set('trailer',$this->input->post('trailer'))
             ->set('quality',$this->input->post('quality'))
             ->set('release_date',$this->input->post('release_date'))
             ->set('status',$this->input->post('status'))
             ->set('themoviedb_id',$this->input->post('themoviedb_id'))
             ->set('user_id',$this->input->post('user_id'))
             ->set('overview',$this->input->post('overview'))
             ->set('duration',$this->input->post('duration'))
             ->set('rating',$this->input->post('rating'))
             ->set('screenshot','screenshot.jpg')
             ->set('poster','poster.jpg')
             ->set('hash',$data['hash'])
             ->set('uri',$data['uri'])
             ->set('size',$data['size'])
            //->set('',$data[''])
             ->update('movies');
        $i = 1;
        $this->db->where('movie_id',$id)->delete('movies_countries');
        foreach($data['country_id'] as $k=>$v)
        {
            $this->db
                ->set('country_id',$v)
                ->set('movie_id',$id)
                ->set('order_id',$i++)
                ->insert('movies_countries');
        }
        $i = 1;
        $this->db->where('movie_id',$id)->delete('movies_actors');
        foreach($data['actors'] as $k=>$v)
        {
            $this->db
                ->set('person_id',$v)
                ->set('movie_id',$id)
                ->set('order_id',$i++)
                ->insert('movies_actors');
        }
        $i = 1;
        $this->db->where('movie_id',$id)->delete('movies_directors');
        foreach($data['director'] as $k=>$v)
        {
            $this->db
                ->set('person_id',$v)
                ->set('movie_id',$id)
                ->set('order_id',$i++)
                ->insert('movies_directors');
        }
        $i = 1;
        $this->db->where('movie_id',$id)->delete('movies_genre');
        foreach($data['genre'] as $k=>$v)
        {
            $this->db
                ->set('genre_id',$v)
                ->set('movie_id',$id)
                ->set('order_id',$i++)
                ->insert('movies_genre');
        }


        $file = $this->get_file($id);
        $new_dir ='upls/files/'.$file['imdb'];


        @mkdir($new_dir);
        @rename($dir."/".$name_file, $new_dir."/".$file['file']);
        @rename($dir."/".$data['poster'], $new_dir."/".$file['poster']);
        @rename($dir."/".$data[$this->input->post('screenshot')], $new_dir."/".$file['screenshot']);


    }
    function save_movie($data,$dir,$id = null)
    {

        $name_file = $this->input->post('file');
        $ext = pathinfo($name_file, PATHINFO_EXTENSION);
        $namefile = 'video';//url_title(mb_strtolower(convert_accented_characters($data['title'])));
        $data['file'] = $namefile.'.'.$ext;
        $data['size'] = filesize($dir.'/'.$name_file);
        $poster = 'poster.jpg';
        $screenshot  = 'screenshot.jpg';
        if (empty($this->input->post('screenshot'))) {

            $movie = new ffmpeg_movie($dir.'/'.$name_file);
            $s = (int)($movie->getFrameCount()*2.5/100);
            $image = $movie->getFrame($s);
            $show_img = $image->toGDImage();
            $_POST['screenshot'] = sprintf('screenshot_%s.jpg',$this->input->post('imdb'));
            imagejpeg($show_img,$dir.'/'.$this->input->post('screenshot'));

        }
             $this->db
                  ->set('published',$this->input->post('published'))
                  ->set('imdb',$this->input->post('imdb'))
                  ->set('title',$this->input->post('title'))
                  ->set('original_language',$this->input->post('original_language'))
                  ->set('original_title',$this->input->post('original_title'))
                  ->set('trailer',$this->input->post('trailer'))
                  ->set('quality',$this->input->post('quality'))
                  ->set('release_date',$this->input->post('release_date'))
                  ->set('status',$this->input->post('status'))
                  ->set('themoviedb_id',$this->input->post('themoviedb_id'))
                  ->set('user_id',$this->input->post('user_id'))
                  ->set('overview',$this->input->post('overview'))
                  ->set('duration',$this->input->post('duration'))
                  ->set('rating',$this->input->post('rating'))
                  ->set('screenshot',$screenshot)
                  ->set('file',$data['file'])
                  ->set('poster',$poster)
                  ->set('hash',$data['hash'])
                  ->set('uri',$data['uri'])
                  ->set('size',$data['size'])
                  //->set('',$data[''])
                  ->insert('movies');
        $id = $this->db->insert_id();
        $i = 1;
        foreach($this->input->post('country_id') as $k=>$v)
        {
           $this->db
                ->set('country_id',$v)
                ->set('movie_id',$id)
                ->set('order_id',$i++)
                ->insert('movies_countries');
        }
        $i = 1;
        foreach($this->input->post('actors') as $k=>$v)
        {
           $this->db
                ->set('person_id',$v)
                ->set('movie_id',$id)
                ->set('order_id',$i++)
                ->insert('movies_actors');
        }
        $i = 1;
        foreach($this->input->post('director') as $k=>$v)
        {
           $this->db
                ->set('person_id',$v)
                ->set('movie_id',$id)
                ->set('order_id',$i++)
                ->insert('movies_directors');
        }
        $i = 1;
        foreach($this->input->post('genre') as $k=>$v)
        {
           $this->db
                ->set('genre_id',$v)
                ->set('movie_id',$id)
                ->set('order_id',$i++)
                ->insert('movies_genre');
        }
        $file = $this->get_file($id);
        $new_dir ='upls/files/'.$file['imdb'];

        @mkdir($new_dir);
        if(is_file($dir."/".$name_file))
        {
            @rename($dir."/".$name_file, $new_dir."/".$file['file']);
        }
        if(is_file($dir."/".$this->input->post('poster')))
        {
            @rename($dir."/".$this->input->post('poster'), $new_dir."/".$file['poster']);
        }

        if($dir."/".$this->input->post('screenshot'))
        {
            @rename($dir."/".$this->input->post('screenshot'), $new_dir."/".$screenshot);
        }







    }
    function get_persons($credits = array())
    {
      $data = array('actors'=>array(),'director'=>array(),'producer'=>array());
      $jobs = array('director','producer');

      if(isset($credits->cast))
      {
       foreach ( array_slice($credits->cast,0,5) as $k=>$v)
       {
         $this->get_person($v,$data,'actors');
         usleep(500000);
       }
       foreach ($credits->crew as $k=>$v)
       {
           if(in_array(mb_strtolower($v->job),$jobs))
           {

               $this->get_person($v,$data,mb_strtolower($v->job));
               usleep(500000);
           }

       }
          return $data;
      }
        return false;
    }
    function get_person($person_obj = null,&$data = array(),$type)
    {
        $this->load->library('moviedb');
        if(isset($person_obj->name))
        {
           $result = $this->db
                          ->where('name',$person_obj->name)
                          ->from('persons')
                          ->get()
                          ->row();
           if($result == null)
            {
              $person = $this->moviedb->get_person($person_obj->id);
              $photo = "{$person->imdb_id}.jpg";
              $dir = "upls/persons/{$person->imdb_id}";
              @mkdir($dir);
              $image_person = $person->imdb_id.'.jpg';
              if($this->moviedb->save_poster($image_person,$person->profile_path,$dir) == false)
              {
                  $photo = '';
              }
              $this->db
                   ->set('name',$person->name)
                   ->set('gender',$person->gender)
                   ->set('imdb',$person->imdb_id)
                   ->set('themoviedb_id',$person->id)
                   ->set('photo',$photo)
                   ->set('uri', $this->get_is_uri($person->name, null, 'persons'))
                   ->insert('persons');
              $data[$type][$this->db->insert_id()] = $person->name;
            }
            else
              $data[$type][$result->id] = $person_obj->name;


        }
        return false;
    }
    function get_country($str)
    {

     $data = array();
     foreach (explode('/',$str) as $k=>$v)
     {
         $country = trim($v);
         $data[$this->set_get_country($country)] = trim($country);
     }

        return $data;

    }
    function set_get_country($str)
    {
        $result = $this->db
                       ->where('name',$str)
                       ->get('countries')
                       ->row();

        if($result == null)
        {
            $this->db
                 ->set('value_de',$str)
                 ->set('name',$str)
                ->set('uri', $this->get_is_uri($str, null, 'countries'))
                 ->insert('countries');
            return $this->db->insert_id();
        }
        else
        {
            return $result->id;
        }
    }
    function get_genre($id = array(),$lang = 'de')
    {
        $data = array();
        if(!empty($id))
        {
            $data =  $this->db
                            ->where_in('themoviedb_id',$id)
                            ->from('genre')
                            ->get()
                            ->result();
        }

        return $this->get_list($data,"name",false);
    }

    function is_unique_imdb($imdb,$id)
    {
        return  $this->db
                       ->where('imdb',$imdb)
                       ->where('id !=',$id)
                       ->from('movies')
                       ->get()
                       ->num_rows() ===0?true:false;
                       
                       
                       
    }

    
    
    
     
}
