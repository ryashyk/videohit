<?php

class bk_crud_model extends grocery_CRUD_Model{

    //protected $_order_by = '';
    //protected $_where = array();

    function get_list(){
        $where = array();

        if($this->_where){
            $this->_where = array_unique($this->_where);

            $where[] = "(".implode(' AND ', $this->_where).")";
        }

        if($this->_or_where){
            $this->_or_where = array_unique($this->_or_where);

            $where[] = "(".implode(' OR ', $this->_or_where).")";
        }

        if($this->_like){
            $this->_like = array_unique($this->_like);

            $where[] = "(".implode(' OR ', $this->_like).")";
        }

        if($where){
            $where = "WHERE ".implode(' AND ', $where);
        }else{
            $where = "";
        }

        $order = "";

        if($this->_order_by){
            $order = "ORDER BY ".implode(', ', $this->_order_by);
        }

        return $this->db->query("SELECT `bk`.`id`, `bk`.`name`, `bk`.`active`  FROM `bk` ".$where." ".$order." ".$this->_limit)->result();
    }

    function get_total_results(){
        //return $this->db->count_all_results($this->table_name);

        $where = array();

        if($this->_where){
            $this->_where = array_unique($this->_where);

            $where[] = "(".implode(' AND ', $this->_where).")";
        }

        if($this->_or_where){
            $this->_or_where = array_unique($this->_or_where);

            $where[] = "(".implode(' OR ', $this->_or_where).")";
        }

        if($this->_like){
            $this->_like = array_unique($this->_like);

            $where[] = "(".implode(' OR ', $this->_like).")";
        }

        if($where){
            $where = "WHERE ".implode(' AND ', $where);
        }else{
            $where = "";
        }

        $data = $this->db->query("SELECT COUNT(`bk`.`id`) as `count` FROM (`bk`) ".$where)->row();

        if($data != null){
            return (int)$data->count;
        }

        return 0;
    }

    public $skip = array(
        'bk.category'
    );

    public $exclusion = array(
    );

    protected $_order_by = array();

    function order_by($order_by, $direction){
        if(!in_array($order_by, $this->skip) || in_array($order_by, $this->exclusion)){
            $this->_order_by[] = $order_by." ".$direction;
        }

        //$this->db->order_by($order_by, $direction);
    }

    protected $_limit = "";

    function limit($limit = 0, $offset = 0){
        if($limit > 0){
            $this->_limit = "LIMIT ".$offset.", ".$limit;
        }

        //$this->db->limit($limit, $offset);
    }

    protected $_where = array();

    function where($key = NULL, $value = NULL, $escape = TRUE){
        if(!in_array($key, $this->skip)){
            $this->_where[] = $key." = '".$value."'";

            //$this->db->where($key, $value, $escape);
        }
    }

    protected $_or_where = array();

    function or_where($key, $value = NULL, $escape = TRUE){
        if(!in_array($key, $this->skip)){
            $this->_or_where[] = $key." = '".$value."'";

            //$this->db->or_where($key, $value, $escape);
        }
    }

    protected $_like = array();

    function like($field, $match = '', $side = 'both'){
        if(stripos($field, '.') === false){
            $field = $this->table_name.'.'.$field;
        }

        if(!in_array($field, $this->skip)){
            $this->_like[] = $field." LIKE '%".$match."%'";

            //$this->db->like($field, $match, $side);
        }
    }

    function or_like($field, $match = '', $side = 'both'){
        if(stripos($field, '.') === false){
            $field = $this->table_name.'.'.$field;
        }

        if(!in_array($field, $this->skip)){
            $this->_like[] = $field." LIKE '%".$match."%'";

            //$this->db->or_like($field, $match, $side);
        }
    }
}
