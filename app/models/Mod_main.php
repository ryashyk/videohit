<?php
class Mod_main extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    } 
    public function settings()
    {
       $data = (object)array();
       $result =  $this->db
                       ->select("field,value,end")
                       ->where('autoload','1') 
                       ->get('settings')
                       ->result();
       foreach($result as $k=>$v)
       {
         $data->{$v->field} = $v->value.$v->end;
       }
       return $data;
    }

 
}
