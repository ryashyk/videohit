<?php

class Movies_Grocery_CRUD_Model extends Grocery_CRUD_Model
{
    public $unset_field = array('nid','publish','user');


    function where($key, $value = NULL, $escape = TRUE)
    {
        if (!in_array($key, $this->unset_field)) 
        {

            $this->db->where($key, $value, $escape);
        }

    }

    function or_where($key, $value = NULL, $escape = TRUE)
    {
        if (!in_array($key, $this->unset_field))
        {
            $this->db->or_where($key, $value, $escape);
        }

    }

    function like($key, $value = NULL, $escape = TRUE)
    {
        //$this->firephp->log($key);
        if (!in_array($key, $this->unset_field))
        {

            $this->db->like($key, $value, $escape);
        }

    }

    function or_like($key, $match = '', $side = 'both')
    {

        if (!in_array($key, $this->unset_field))
        {

            $this->db->or_like($key, $match, $side);
        }
    }


}