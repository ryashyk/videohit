<?php defined('BASEPATH') or exit('No direct script access allowed');
class MY_Controller extends CI_Controller{
    
    public $group_id = null;
    public $user = null;
    public $menu;
    public $id;
    public $admin_path = 'admin';
    public $admin_dir = "admin";
    public $active;
    public $field = array();
    public $cache_array = array();
    public function __construct()
    {
        parent::__construct();
        $this->lang->load('this_site');    
        //$this->cache->restart(array('adapter' => 'memcached', 'backup' => 'file'));         
        $this->menu = array();
    }
    function _get_data_movie($imdb = null,$dir = null,$cron = false)
    {

        $this->load->library('imdb');
        $this->imdb->fetchUrl('http://www.imdb.de/title/'.$imdb);
        $this->load->library('moviedb');
        $moviedb = $this->moviedb->get_movie($imdb);
        $this->load->model('mod_admin');
        $poster_img = 'poster_'.$imdb.'.jpg';
        $screenshot_img  = 'screenshot_'.$imdb.'.jpg';

         if($moviedb)
        {
            $poster = site_url($this->moviedb->save_poster($poster_img,$moviedb->poster_path,$dir));
            if($patch_screenshot = $this->moviedb->get_screenshot($imdb,2))
            {
                $screenshot = site_url($this->moviedb->save_poster($screenshot_img,$patch_screenshot,$dir));
            }
            else
            {
                $screenshot = false;
            }

            $this->field = $this->mod_admin->get_persons($this->moviedb->get_credits($moviedb->id));
            $this->field['themoviedb_id'] = $moviedb->id;
            $this->field['overview'] = $moviedb->overview;
            if($screenshot)
            {
                $this->field['screenshot'] = $cron?$screenshot_img:[
                    'success' => true,
                    'files'=>[
                        'name'=>$screenshot_img,
                        'url'=>$screenshot,
                    ],
                ];
            }
            if($moviedb->poster_path)
            {
                $this->field['poster'] = $cron?$poster_img:[
                    'success' => true,
                    'files'=>[
                        'name'=>$poster_img,
                        'url'=>$poster,
                    ],
                ];
            }
            $duration = explode("|",$this->imdb->getRuntime());
            $this->field['title'] = $moviedb->title;
            $this->field['original_title'] = $moviedb->original_title;
            $this->field['release_date'] = $moviedb->release_date;
            $this->field['genre']  = $this->mod_admin->get_genre($moviedb->genre_ids);
            $this->field['country_id'] = $this->mod_admin->get_country($this->imdb->getCountry());
            $this->field['original_language'] = $moviedb->original_language;
            $this->field['rating'] = $this->imdb->getRating();
            $this->field['duration'] = isset($duration[0])?$duration[0]:null;
            $this->field['trailer'] = $this->moviedb->get_videos($moviedb->id);
        }
      
        return $this->field;
    }
    function _imdb($imdb)
    {
        $this->firephp->log($imdb);
        $this->load->library('form_validation');
        if (!$this->mod_admin->is_unique_imdb($imdb, $this->id))
        {
            $this->form_validation->set_message('_imdb', 'This imdb already exists');
            return false;
        }
        $this->load->library('imdb');
        $this->imdb->fetchUrl('http://www.imdb.com/title/'.$imdb);
        if ($this->imdb->isReady&&(bool)preg_match('(^tt[0-9]{7}$)',$imdb))
        {
            return true;
        } else
        {
            $this->form_validation->set_message('_imdb', 'This Imdb not valid');
            return false;
        }
    }


}


class Admin_Controller extends MY_Controller
{

    public $data; //Данные вида
    public $current_section; //Текущая страница
    public $breadcrumbs = array(); //Хлебный крошки
    public $filter_fields = array();
    public $js_files = array();
    public $css_files = array();
    public $table_bd;
    public $filed_url_generation = 'title';
    public $unset_field = array();
    

    function __construct()
    {
        parent::__construct();
        $this->load->library('ion_auth');
        
        if ($this->ion_auth->logged_in()==false) {
            redirect('admin/auth'); 
        }
        $this->user =  $this->ion_auth->user()->row();         
        $this->load->model('mod_admin');
        $this->mod_admin->cache_array = $this->cache_array;
        $this->data =  new stdClass();
        $this->breadcrumbs['admin'] = lang('admin panel');
        $this->load->library('grocery_CRUD');        
        $this->crud = new grocery_CRUD(); //Подключаю CRUD
        if($this->user->group_id == '1')
        {
            $this->menu[$this->admin_path.'/users'] = array('label' => ucfirst(lang('users')), 'parent_id' => $this->admin_path.'/settings');
        }
        else
        {
            $this->menu[$this->admin_path.'/users/index/edit/'.$this->user->id] = array('label' => ucfirst(lang('users')), 'parent_id' => $this->admin_path.'/settings');
        }
        $this->load->model('mod_admin', 'mod_admin', true);
        $this->crud->set_theme('flexigrid');
        $this->crud->display_as('description', lang('description'));
        $this->crud->display_as('keywords', lang('keywords'));
        $this->crud->display_as('status', lang('status'));
        $this->crud->field_type('status', 'true_false');
        $this->crud->display_as('date', lang('date'));
        $this->crud->display_as('text', lang('text'));
        $this->crud->display_as('uri', lang('uri'));
        $this->menu['/'] = lang('site');
        $this->menu[$this->admin_path] = array('label' => lang('admin panel'));
        $this->menu[$this->admin_path.'/settings'] = array('label' => ucfirst(lang('settings')));
        $this->menu[$this->admin_path.'/lang'] = array('label' => ucfirst(lang('lang')), 'parent_id' => $this->admin_path.'/settings');
        $this->menu[$this->admin_path.'/movies'] = array('label' => ucfirst(lang('movies')));
        $this->menu[$this->admin_path.'/movies/unpublished'] = array('label' => ucfirst(lang('unpublished')));
        $this->menu[$this->admin_path.'/pages'] = array('label' => ucfirst(lang('pages')));
        $this->menu[$this->admin_path.'/movies/downloads'] = array('label' => "DW");

    }
    function _example_output()
    {
      $this->crud->set_table($this->table_bd);
      $this->data = $this->crud->render();
      $this->data->group_id = $this->group_id;
      $this->data->old_action = $this->crud->getState();
      $this->data->breadcrumbs = $this->breadcrumbs;
      $this->data->current_section = $this->current_section;
      $this->data->js_files = $this->data->js_files + $this->js_files;
      $this->data->css_files = $this->data->css_files + $this->css_files;
      $this->data->user = $this->user;
      $this->data->admin_path = $this->admin_path;
      $this->data->admin_dir = $this->admin_dir;
      $this->tpl->set('menu', $this->menus->render($this->menu, $this->active, null, 'basic'))
                ->set_view('content', 'admin/panel')
                ->build('admin/main', $this->data);
    }
    public function _callback_filter($data, $id = 'null')
    {
        
         if (isset($data['uri'])) {
         $data['uri'] = $this->mod_admin->get_is_uri($data[$this->filed_url_generation], $data['uri'], $this->
                table_bd, $id);
        }
                foreach ($data as $k => &$v) {
 if (array_key_exists($k, $this->filter_fields)) {
                $filters = $this->filter_fields[$k];
                foreach ($filters as $key => $val) {
                    if (function_exists($val) and $val != 'htmlspecialchars') {
                        eval("\$data[\$k]  = \$val(\$v);");
                                       } elseif (function_exists($val) and $val == 'htmlspecialchars') {
                 
                        $data[$k] = htmlspecialchars($v, ENT_QUOTES);
                        
                    }
                  
                }


            }
       
       if(!is_array($v))
       {
         $data[$k] = $this->encode_php_tags(trim($v));
       }     
       else
       {
        foreach ($v as $ks=>$vs) {
          $data[$k][$ks] = $this->encode_php_tags(trim($vs));
        }
       }
         } 
        foreach ($this->unset_field as $key=>$val) {
        unset($data[$val]);
       }
         
           return $data;
    }
  
    
   
    public function encode_php_tags($str)
	{
		return str_replace(array('<?php', '<?PHP', '<?', '?>'),  array('&lt;?php', '&lt;?PHP', '&lt;?', '?&gt;'), $str);
	}
}

class Public_Controller extends MY_Controller
 {

    public $offset = null;
    public $online = false; 
    function __construct()
    {
       parent::__construct();
     
           
       
       
       
      /* if(! $this->menu = $this->cache->get('menu'))
      {
        $this->menu = $this->mod_main->get_pages();
        $this->cache->save('menu', $this->menu, 36000);  
      }*/
       
       $this->per = 16;
       $this->tpl
            ->set('menu',$this->menu)
            ->set('active',null)
            ->set_breadcrumbs(array())
            ->set('online',$this->online)
            ->set('content',null)
            ->set('title',null)
            ->set('keywords',null)
            ->set('description',null);
    
      }
 }   
