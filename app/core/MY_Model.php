<?php
class MY_Model extends CI_Model
{
    public $time_online = 90;

    public function __construct()
    {
        parent::__construct();
    }  
    public function get_list_fields($table,$field = 'name', $first_null = true,$key = 'id')
    {
         $result = $this->db
                        ->get($table)
                        ->result();
         return $this->get_list($result,'name');
    }
    public function get_setting($field)
    {
       $result =  $this->db
       ->select('id,value,field,end')
       ->where('field',$field) 
       ->get('settings')
       ->row();
       return $result; 
    }
    public function get_setting_value($field)
    {
       $result =  $this->db
       ->select("id,value,field,end")
       ->where('field',$field) 
       ->get('settings')
       ->row();
      if($result == null)
      {
        return null;
      }
       return $result->value; 
    } 
    function get_list($obj, $field = 'name', $first_null = true,$key = 'id',$sort = true)
    {
        $data = array();
        if ($first_null) {
            $data[null] = null;
        }

        foreach ($obj as $k => $v) {
            $data[$v->{$key}] = $v->{$field};
        }
        if($sort)
        {
            asort($data);
        }
        

        return $data;
    }
    public function check_fields_bd($array, $table)
    {
        $fields = $this->db->list_fields($table);
        $data = array();
        foreach ($array as $k => $v) {
            if (in_array($k, $fields)) {
                $data[$k] = $v;
            }
        }
        return $data;
    }
    
    function hash_uri($name, $uri, $tb, $id = 'null',$field='uri')
    {
       
        if (empty($uri)) {
            $uri = substr(md5(rand()), 0, 8);  
        }
        else{
            $uri = substr(url_title(mb_strtolower(convert_accented_characters($uri))), 0, 8);
        } 
        
        
        $uris = $uri;
        $unuri = $this->uri_is_hash($uri, $tb, $id,$field);
        while ($unuri > 0) {
           $uris =  substr(md5(serialize($uri).rand()), 0, 8);
           $unuri = $this->uri_is_hash($uris, $tb, $id,$field);
        }
        return $uris;
    }
    function get_is_uri($name, $uri, $tb, $id = 'null',$field="uri")
    {
        $uri = trim($uri);

        if (empty($uri)) {
             $uri = url_title(mb_strtolower(convert_accented_characters(trim($name))));
        } else {
            $uri = url_title(mb_strtolower(convert_accented_characters($uri)));
        }
        $i = 0;
        $uris = $uri;
        $unuri = $this->uri_is($uri, $tb, $id,$field);
        while ($unuri > 0) {
            
            preg_match('/^(.*)-([0-9]+)$/', $uri, $cleanurl);
            if(!empty($cleanurl))
            {
              $uri = $cleanurl[1]; 
              $i = $cleanurl[2];
               
            }
            $i++;
          
            $uris =  $uri. '-' . $i;
            $unuri = $this->uri_is($uris, $tb, $id,$field);
        }
        return $uris;
    }

    private function uri_is($uri, $tb, $id = 'null',$field="uri")
    {
        return $this->db->where('id !=', $id)->where($field, $uri)->get($tb)->num_rows();
    }
    private function uri_is_hash($uri, $tb, $id = 'null',$field="uri")
    {
        $result = $this->db->where('id !=', $id)->where($field, $uri)->get($tb)->num_rows();
       
        if($result==0)
        {
           $result = $this->db->where($field, $uri)->get('old_uri')->num_rows();
            
        }
        
       return $result;
    }

}
