//	JavaScript API 2.0 for Uppod 1+
//  http://uppod.ru/js

	// Events
	
	function uppodEvent(playerID,event) { 

		switch(event){
		
			case 'init': 
				console.log('init');
				break;
				
			case 'start': 
				console.log('start');
				break;
			
			case 'play': 
				console.log('play');
				break;
				
			case 'pause': 
				btn_play.style.display = 'block';
				break;
				
			case 'stop': 
				console.log('stop');
				break;
				
			case 'seek': 
				console.log('seek');			
				break;
				
			case 'loaded':
				console.log('loaded');
				break;
				
			case 'end':
				console.log('end');
				break;
				
			case 'download':
				console.log('download');
				break;
				
			case 'quality':
				console.log('quality');
				break;
			
			case 'error':
				console.log('error');
				break;
					
			case 'ad_end':
				console.log('ad_end');
				break;
				
			case 'pl':
				console.log('pl');
				break;
			
			case 'volume':
				console.log('volume');
				break;
		}
		
	}
	
	// Commands
	
	function uppodSend(playerID,com,callback) {
		document.getElementById(playerID).sendToUppod(com);
	}
	
	// Requests
	
	function uppodGet(playerID,com,callback) {
		return document.getElementById(playerID).getUppod(com);
	}

