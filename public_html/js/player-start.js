
var flash = false,
	browser = cBrowser(),
	videoUrl = document.getElementById('video').getAttribute('data-url'),
	videoUrlEncode = document.getElementById('video').getAttribute('data-url-encode'),
	type = 'html',
    changeElm = document.querySelectorAll('.change_type');

function testPlayer(V_URL) {

    /*
    CodoPlayer(V_URL, {
        width: 800,
        height: 480,
        engine:'html5',
        //preload: false,
        controls: true,
    }, "#video");
    */


    var uppod = new Uppod({
        m:"video",
        uid:"video",
        file: V_URL,
        st:"uppodvideo",
        width : 800,
        height : 480,
    });


	/*
    var flashvars = {
        "st"	: "http://videohit.to/styles/video206-1302.txt",
        "uid"	: "video",
        "file" 	: V_URL,
        // 'poster': "<?=image_thumb("/upls/files/{$file->hash}/{$file->img}",800,480,true);?>"
    };

    var params = {
        bgcolor:"#ffffff",
        wmode:"transparent",
        width : 800,
        height: 480,
        allowFullScreen:"true",
        allowScriptAccess:"always"
    };

    swfobject.embedSWF("http://videohit.to/player/uppod.swf", "video", "800", "480", "10.0.0.0", false, flashvars, params);
    */
}



function destroyFlash() {
    document.getElementById('video').parentNode.removeChild(document.getElementById('video'));
    document.getElementById('mediaspace').insertAdjacentHTML('beforebegin', '<div id="video" class="video-blk" data-url="'+videoUrl+'"></div>');
}

function toggleType(EL , TYPE) {

    if (EL.parentNode.classList) {
        EL.parentNode.classList.add('active');
    } else {
        EL.parentNode.className += ' ' + 'active';
    }

    if (TYPE !== type) {
        startFlash(videoUrl);
    } else {
        destroyFlash();
        startHtml(videoUrl);
    }

}

Array.prototype.forEach.call(changeElm, function(el, i){

    el.onclick = function() {

        var activeElm = document.querySelector('.video-nav .active');

        if (activeElm.classList) {
            activeElm.classList.remove('active');
        } else {
            activeElm.className = activeElm.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
        }

        toggleType( el , el.getAttribute('data-type'))
    }
});




function startHtml(V_URL) {
	var uppod = new Uppod({
	    m:"video",
	    uid:"video",
	    file: V_URL,
	    st:"uppodvideo",
	    width : 800,
	    height : 480,
	});
}

function startFlash(V_URL) {

	var flashvars = {
		"st"	: "http://videohit.to/styles/video206-1302.txt",
		"uid"	: "video",
		"file" 	: V_URL,
		// 'poster': "<?=image_thumb("/upls/files/{$file->hash}/{$file->img}",800,480,true);?>"
	};

	var params = {
		bgcolor:"#ffffff", 
		wmode:"transparent", 
		width : 800,
		height: 480,
		allowFullScreen:"true", 
		allowScriptAccess:"always"
	}; 

	swfobject.embedSWF("http://videohit.to/player/uppod.swf", "video", "800", "480", "10.0.0.0", false, flashvars, params);

    /*
	document.getElementById('video').style.display = 'none';
	var s1 = new SWFObject('/js/flw/hdplayer.swf','player','800','480','9',"#333333");
	s1.addParam('allowfullscreen','true');
	s1.addParam('allowscriptaccess','always');
	s1.addParam('WMode','direct');
	s1.addVariable('file',V_URL);
	s1.addVariable('autoplay','false');
	s1.write('mediaspace');
     */
}

function mediaSupport() {
	var test_video = document.createElement("video");
	var mediasupport = { 
		video: (test_video.play)? true : false
	}
	return mediasupport.video;
}

function cBrowser() {
	var ua = navigator.userAgent;

	var bName = function () {
		if (ua.search(/MSIE/) > -1) return "ie";
		if (ua.search(/Firefox/) > -1) return "firefox";
		if (ua.search(/Opera/) > -1) return "opera";
		if (ua.search(/Chrome/) > -1) return "chrome";
		if (ua.search(/Safari/) > -1) return "safari";
		if (ua.search(/Konqueror/) > -1) return "konqueror";
		if (ua.search(/Iceweasel/) > -1) return "iceweasel";
		if (ua.search(/SeaMonkey/) > -1) return "seamonkey";
	}();

	var version = function (bName) {
		switch (bName) {
			case "ie" : return (ua.split("MSIE ")[1]).split(";")[0];break;
			case "firefox" : return ua.split("Firefox/")[1];break;
			case "opera" : return ua.split("Version/")[1];break;
			case "chrome" : return (ua.split("Chrome/")[1]).split(" ")[0];break;
			case "safari" : return (ua.split("Version/")[1]).split(" ")[0];break;
			case "konqueror" : return (ua.split("KHTML/")[1]).split(" ")[0];break;
			case "iceweasel" : return (ua.split("Iceweasel/")[1]).split(" ")[0];break;
			case "seamonkey" : return ua.split("SeaMonkey/")[1];break;
		}
	}(bName);
					   	
	if ((typeof version) != 'undefined'){
		return [bName , version.split(".")[0]];
	} else {
		return ['', 0];
	}
};

switch (browser[0]) {
	case 'chrome':
		if (browser[1] < 42 || !mediaSupport()) {
			flash = true;
		};
	break;
	case 'opera':
		if (browser[1] < 34 || !mediaSupport()) {
			flash = true;
		};
	break;
	case 'firefox':
		if (browser[1] < 42 || !mediaSupport()) {
			flash = true;
		};
	break;
	case 'safari':
		if (browser[1] < 9 || !mediaSupport()) {
			flash = true;
		};
	break;
	case 'ie':
	if (browser[1] < 9 || !mediaSupport()) {
		flash = true;
	};
	break;
};

if (!flash) {
	startHtml(videoUrl);
} else {
	startFlash(videoUrl);
}

// testPlayer(videoUrl);

