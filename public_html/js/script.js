
$(docLoad);

function docLoad() {

    $(document).on('click' , '#btn-login' , function(event) {
        event.preventDefault();
        $('.modal-body').prepend('<div class="alert alert-danger auth-error"> Authorization failed! </div>');
        setTimeout(function() {
            $('.auth-error').fadeOut('slow' ,function () {
                $(this).remove();
            });
        } , 3000);
    });

//	IE script
    if (window.navigator.userAgent.indexOf("MSIE ") > 0) {   
       
    } 
}

$.fn.exists = function(){return this.length>0;}

function getClientWidth() {
	return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientWidth:document.body.clientWidth;
}

function getClientHeight() {
  	return document.compatMode=='CSS1Compat' && !window.opera?document.documentElement.clientHeight:document.body.clientHeight;
}

var isMobile = {
    Android: function() {
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function() {
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function() {
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function() {
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function() {
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function() {
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};
// if( isMobile.any() ) { alert('Mobile') };