 function useFlash() {
 		var flashvars1 = {
			"st"	: "/styles/video206-1287.txt",
			"debug"	: 1,
			"file" 	: "<?=site_url("upls/files/{$file->hash}/{$file->file}");?>",
			'poster': "<?=image_thumb("/upls/files/{$file->hash}/{$file->img}",800,480,true);?>" /*<======= СЮДА ВСТАВИТЬ ССЫЛКУ НА ПРЕВЬЮ ФИЛЬМА*/
		}; 
		var params1 = {
			bgcolor:"#ffffff", 
			wmode:"transparent", 
			width : 800,
			height: 480,
			allowFullScreen:"true", 
			allowScriptAccess:"always"
		}; 
		swfobject.embedSWF("/player/uppod.swf", "videoplayer1", "800", "480", "10.0.0.0", false, flashvars1, params1);
	};

	function useHTML() {
		Uppod({m:"video",uid:"videoplayer1",file:"<?=site_url("upls/files/{$file->hash}/{$file->file}");?>" , poster:"<?=image_thumb("/upls/files/{$file->hash}/{$file->img}",800,480,true);?>" });
	};

	var ua = navigator.userAgent.toLowerCase();
	var flashInstalled = false;

	if (typeof(navigator.plugins)!="undefined"&&typeof(navigator.plugins["Shockwave Flash"])=="object"){
		flashInstalled = true;
	} else if (typeof window.ActiveXObject != "undefined") {
		try {
			if (new ActiveXObject("ShockwaveFlash.ShockwaveFlash")) {
			    flashInstalled = true;
			}
		} catch(e) {};
	};

	if (ua.indexOf("iphone") != -1 || ua.indexOf("ipad") != -1 || ua.indexOf("android") != -1 || ua.indexOf("windows phone") != -1 || ua.indexOf("blackberry") != -1) {
		//код HTML5
		useHTML();
	} else {
		if (!flashInstalled) {
			//просим установить Flash
			document.getElementById("videoplayer1").innerHTML="<a href='http://www.adobe.com/go/getflashplayer'>Требуется обновить Flash-плеер</a>";
			useHTML();
		} else {
			//код Flash (SWFObject)
			useFlash();
		}
	};