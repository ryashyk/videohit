$(document).ready(function() {
	/*$(document).on('click' , '.toogleAside' , function(event) {
		event.preventDefault();
		_this = $(this);
		_body = $('body');
		_thIco = _this.find('i')
		if (_thIco.hasClass('fa-arrow-circle-left')) {
			_thIco.removeClass('fa-arrow-circle-left').addClass('fa-arrow-circle-right ');
		} else {
			_thIco.addClass('fa-arrow-circle-left').removeClass('fa-arrow-circle-right ');
		}
		if (_body.hasClass('hidden-menu')) {
			_body.removeClass('hidden-menu');
		} else {
			_body.addClass('pre-hidden-menu');
			setTimeout(function() {
				_body.addClass('hidden-menu').removeClass('pre-hidden-menu');
			},50)
		}
			
		
		
	})*/

	//  стилизация селектов, радио кнопок и чекбоксов
	setTimeout(function(){
		$(".select").chosen({disable_search: true});
		$('table.dataTable').wrap("<div class='scroll_wrp'><div class='table_wrapper'></div></div>");
	},100);

	var flash = false,
		browser = cBrowser(),
		videoUrl = document.getElementById('video').getAttribute('data-url'),
		videoUrlEncode = document.getElementById('video').getAttribute('data-url-encode');

	function startHtml(V_URL) {
		var uppod = new Uppod({
			m:"video",
			uid:"video",
			file: V_URL,
			st:"uppodvideo",
			width : 800,
			height : 480,
		});
	}

	function startFlash(V_URL) {
        /*
		var flashvars = {
			"st"	: "http://videohit.to/styles/video206-1302.txt",
			"uid"	: "video",
			"file" 	: V_URL,
			// 'poster': "<?=image_thumb("/upls/files/{$file->hash}/{$file->img}",800,480,true);?>"
		};

		var params = {
			bgcolor:"#ffffff",
			wmode:"transparent",
			width : 800,
			height: 480,
			allowFullScreen:"true",
			allowScriptAccess:"always"
		};

		swfobject.embedSWF("http://videohit.to/player/uppod.swf", "video", "800", "480", "10.0.0.0", false, flashvars, params);
		*/
        /*
        CodoPlayer(V_URL, {
            width: 800,
            height: 480
        }, "#video");
        */
        document.getElementById('video').style.display = 'none';
        var s1 = new SWFObject('/js/flw/hdplayer.swf','player','800','400','9',"#333333");
        s1.addParam('allowfullscreen','true');
        s1.addParam('allowscriptaccess','always');
        s1.addParam('WMode','direct');
        s1.addVariable('file',V_URL);
        s1.addVariable('autoplay','false');
        s1.write('mediaspace');
	}

	function mediaSupport() {
		var test_video = document.createElement("video");
		var mediasupport = {
			video: (test_video.play)? true : false
		}
		return mediasupport.video;
	}

	function cBrowser() {
		var ua = navigator.userAgent;

		var bName = function () {
			if (ua.search(/MSIE/) > -1) return "ie";
			if (ua.search(/Firefox/) > -1) return "firefox";
			if (ua.search(/Opera/) > -1) return "opera";
			if (ua.search(/Chrome/) > -1) return "chrome";
			if (ua.search(/Safari/) > -1) return "safari";
			if (ua.search(/Konqueror/) > -1) return "konqueror";
			if (ua.search(/Iceweasel/) > -1) return "iceweasel";
			if (ua.search(/SeaMonkey/) > -1) return "seamonkey";
		}();

		var version = function (bName) {
			switch (bName) {
				case "ie" : return (ua.split("MSIE ")[1]).split(";")[0];break;
				case "firefox" : return ua.split("Firefox/")[1];break;
				case "opera" : return ua.split("Version/")[1];break;
				case "chrome" : return (ua.split("Chrome/")[1]).split(" ")[0];break;
				case "safari" : return (ua.split("Version/")[1]).split(" ")[0];break;
				case "konqueror" : return (ua.split("KHTML/")[1]).split(" ")[0];break;
				case "iceweasel" : return (ua.split("Iceweasel/")[1]).split(" ")[0];break;
				case "seamonkey" : return ua.split("SeaMonkey/")[1];break;
			}
		}(bName);

		if ((typeof version) != 'undefined'){
			return [bName , version.split(".")[0]];
		} else {
			return ['', 0];
		}
	};

	switch (browser[0]) {
		case 'chrome':
			if (browser[1] < 42 || !mediaSupport()) {
				flash = true;
			};
			break;
		case 'opera':
			if (browser[1] < 34 || !mediaSupport()) {
				flash = true;
			};
			break;
		case 'firefox':
			if (browser[1] < 42 || !mediaSupport()) {
				flash = true;
			};
			break;
		case 'safari':
			if (browser[1] < 9 || !mediaSupport()) {
				flash = true;
			};
			break;
		case 'ie':
			if (browser[1] < 9 || !mediaSupport()) {
				flash = true;
			};
			break;
	};



	$(document).on('click', '.modal-toggle', function(event) {
        event.preventDefault();
        
        $('#responsive').modal('show');
        var tmpUrl = $(this).attr('data-url');
		$('#modal-title').html($(this).html());
		$('#modal-url').html($(this).attr('data-url'));
        setTimeout(function() {
			if (!flash) {
				startHtml(tmpUrl);
			} else {
				startFlash(encodeURI(tmpUrl));
			}
        },700);

      
      });

	$(document).on('click', '.wc_publish', function(event) {
		event.preventDefault();
		var $t = $(this),
			url = $t.attr('href');
		
		$.ajax({
			url: url,
			type: 'GET',
			dataType: 'json',
		})
		.done(function(result) {
			if (result.success) {
				$t.closest('tr').find('.modal-toggle').attr('data-url' , result.url);
				$('.inner_body').prepend('<div id="rd-success" class="report-div success err-show">Publish successful!</p></div>');
				$('#rd-success').slideDown();
				$t.remove();
			} else {
				$.each(result.fields, function(index, val) {
					$('.inner_body').prepend('<div id="'+index+'" class="report-div error err-show">'+val+'</p></div>');
				});
				$('.err-show').slideDown();
			}
			setTimeout(function() {
				$('.inner_body').find('.err-show').slideUp(function () {
					$(this).remove();  
				})
			}, 5000);
		})
		.fail(function() {
			console.log("error");
		});
		

	});


//	toogle_menu
	// $(document).on('click', '.toogle_menu', function(event) {
	// 	event.preventDefault();
	// 	$('body').toggleClass('menu_close');
	// });

	// $(document).on('click' , '#toogleAside' , function(event) {
	// 	event.preventDefault();
	// 	_this = $(this);
	// 	_body = $('body');
	// 	_thIco = _this.find('i')
	// 	if (_thIco.hasClass('fa-arrow-circle-left')) {
	// 		_thIco.removeClass('fa-arrow-circle-left').addClass('fa-arrow-circle-right ');
	// 	} else {
	// 		_thIco.addClass('fa-arrow-circle-left').removeClass('fa-arrow-circle-right ');
	// 	}
	// 	if (_body.hasClass('menu_close')) {
	// 		_body.removeClass('menu_close');
	// 	} else {
	// 		_body.addClass('pre-hidden-menu');
	// 		setTimeout(function() {
	// 			_body.addClass('menu_close').removeClass('pre-hidden-menu');
	// 		},50)
	// 	}
			
		
		
	// })


//	menu
	$('#left-side-panel').find('li a').each(function() {
		_this = $(this);
		if (_this.parent('li').find('ul').length>0) {
			_this.append('<b class="collapse_sighn"><em class="fa fa-minus-square-o"></em></b>')
		}
	})

	$('#left-side-panel').find('li a').click(function(event) {
		_this = $(this);
		_this.parent('li').siblings('li')
			.removeClass('open')
			.children('a')
			.find('.fa-plus-square-o')
			.addClass('fa-minus-square-o')
			.removeClass('fa-plus-square-o');
		if (_this.parent('li').find('ul').length>0) {
			event.preventDefault();
			_this.parent('li')
				.toggleClass('open')
				.find('.fa-minus-square-o')
				.toggleClass('fa-plus-square-o')

		}
	})

	if ($(window).width()<1024) {
		$('body').toggleClass('menu_close');
	}

	$(window).resize(function() {
		if ($(window).width()<1024) {
			$('body').toggleClass('menu_close');
		} else {
			$('body').toggleClass('menu_close');
		}
	});

	function firstToUpperCase( str ) {
	    return str.substr(0, 1).toUpperCase() + str.substr(1);
	}

	$('.wc-regenerate-mov').parent('a').click(function(event) {
		event.preventDefault();
		var t 	= $(this),
			url = t.attr('href'),
			nameAry = [];
			
		result = confirm('Do you want to regenerate the url?');

		if (result) {
			$.ajax({
				url: url ,
				type: 'GET',
				dataType: 'json',
			})
			.done(function(result) {
				console.log(result)
				if (result.success == true) {

					t.closest('table').find('thead').find('th').each(function(indexTable, elTable) {
						var text = $.trim($(this).text())
						$.each(result, function(ind, val) {

							 if (text == firstToUpperCase( ind )) {
							 	t.closest('tr').find('td:nth-child('+(parseInt(indexTable + 1))+')').html(val);
							 };

						});
					});
					
				};
			})
			.fail(function() {
				console.log("error");
			});
			
		};
	});


	$(document).on('click', '.embed_button', function(event) {
		event.preventDefault();
		var $t = $(this),
			embed = $t.siblings('.embed_html').html();
			
		var c = $('<div class="box-modal" />');

     	c.html(embed);
		c.prepend('<div class="box-modal_close arcticmodal-close" style="cursor:pointer;float:right;font-size:15px;">x</div>');

		$.arcticmodal({
			content: c
		});	
		
	});

	if ( $('.main_stat_list').length ) {

		function parseStrind(string) {
			var parse = string.replace(/\D+/g,"");
			var arr = parse.split("");
			var arrLast = arr[arr.length - 1];
			arr[arr.length - 1] = '.';
			arr.push(arrLast);
			return parseFloat(arr.join(''));
		};

		var options = {
		    useEasing : true, 
		    useGrouping : true, 
		    separator : ',', 
		    decimal : '.', 
		    prefix : '', 
		    suffix : '' 
		};
		
		var countUp1 = new CountUp("count1", 0, parseStrind(count1), 1, 2.5, options);
		var countUp2 = new CountUp("count2", 0, parseStrind(count2), 1, 2.5, options);
		var countUp3 = new CountUp("count3", 0, parseStrind(count3), 1, 2.5, options);
		var countUp4 = new CountUp("count4", 0, count4, 0, 2.5, options);
		var countUp5 = new CountUp("count5", 0, count5, 0, 2.5, options);

		countUp1.start();
		countUp2.start();
		countUp3.start();
		countUp4.start();
		countUp5.start();

	};


	if ($('#list_table').length) {
        $('#list_table').DataTable();
    };
	



});

