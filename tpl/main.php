<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Document</title>
    <link href="/css/style.css" rel="stylesheet" type="text/css">
    <!--[if lte IE 9]>
	    <script type="text/javascript" src="/js/html5shiv.min.js"></script>
	    <script type="text/javascript" src="/js/respond.min.js"></script>
	<![endif]-->
</head>

<body>

	<div class="modal fade" id="modal-login">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="loginform" class="form-horizontal" role="form">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Sign In</h4>
					</div>
					
					<div class="modal-body">
						
		                                    
		                <div style="margin-bottom: 15px" class="input-group">
		                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
		                    <input id="login-username" type="text" class="form-control" name="username" value="" placeholder="Email">
		                </div>
		                              
		                <div style="margin-bottom: 15px" class="input-group">
		                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
		                    <input id="login-password" type="password" class="form-control" name="password" placeholder="Password">
		                </div>
		                                                        
					</div>

					<div class="modal-footer">
						<button id="btn-login" class="btn btn-primary btn-ebmed">Login  </button>
					</div>

				</form>
			</div>
		</div>
	</div>


	<!-- header -->
	<header id="header">
		<div class="container">
			<div class="row">
				<div class="logo">
					<a href="<?=site_url();?>">
						<img src="/img/logo.png" alt="">
					</a>
				</div>
				<nav class="hdr-nav">
					<ul>
						<?php foreach($this->db->where('type','top')->where('status','1')->get('pages')->result() as $k=>$v ):?>
						<li><a href="<?=site_url('page/'.$v->uri);?>"><?=$v->name;?></a></li>
						<?php endforeach;?>
						<li><a class="btn btn-primary" data-toggle="modal" href='#modal-login'>Sign in </a></li>
					</ul>
				</nav>
			</div>
		</div>
	</header>
	<!-- .header -->


	<!-- content -->
	<div id="main">

        <div class="container">
            <div class="row">

            	<?=$content;?>

            </div>
        </div>

	</div>
	<!-- .content -->

	<!-- footer -->
    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="ftr-logo">
                    <a href="<?=site_url();?>">
                        <img src="../img/logo.png" alt="">
                    </a>
                    <span class="ftr-copy">
                        &copy; VIDEOHIT.TO, 2016
                    </span>
                    <span class="copyright">
                        All rights reserved
                    </span>
                </div>
                <div class="ftr-list pull-right">
                    <ul>
                    <?php foreach($this->db->where('type','foot')->where('status','1')->get('pages')->result() as $k=>$v ):?>
                        <li><a href="<?=site_url('page/'.$v->uri);?>"><?=$v->name;?></a></li>
                    <?php endforeach;?>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- .footer -->

	
	<!-- scripts -->
	<script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script> 
	<script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/jquery.parallax.js"></script>
    <script type="text/javascript" src="/js/script.js"></script>
	<!-- .scripts -->

</body>
</html>