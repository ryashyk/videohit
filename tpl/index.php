<div class="upload-content">
    <div class="upload-decor">
        <img src="../img/upload-bg.png" alt="">
        <div class="upload-holder">
            <input type="file">
            <img src="../img/upload-btn.png" alt="">
            <h1>Upload any File</h1>
            <p>Browse or Drag n Drop files</p>
        </div>
    </div>
</div>

