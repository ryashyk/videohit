<!-- 
<script type="text/javascript" src="/js/admin/jquery.fileupload.js"></script>
 -->
<span class="fileinput-button qq-upload-button">
	<span>Загрузить файл</span>
	<input  
		data-url="/admin/movies/upload/<?=$id;?>"
		type="file" 
		name="file" 
		id="file_upload" 
		formenctype="application/x-www-form-urlencoded" 
		formmethod="post"
	/>
</span>
<div id="video_result"><?=$file;?></div>

<div class="meter_wrapper" id="meter_wrapper">
	<div class="meter" id="progress">
		<span class="progress-bar"></span>
	</div>
</div>

<div class="upload_result" id="upload_result"><?=$field;?></div>
<script type="text/javascript" src="/js/admin/jquery.js"></script>
<script>
	var jQ = $.noConflict(true);
	$(document).ready(function() {

		if ($('#file_upload').length) {

			function deleteImageAnchor() {
				$(document).on('click', '.delete-image-anchor', function(event) {
					event.preventDefault();
					$(this).closest('.form-input-box').find('.upload-success-url').hide();
					$(this).closest('.form-input-box').find('.fileinput-button').show();
				});
			}

			function file_upload() {

				$('#file_upload').fileupload({
					dataType: 'json',
				    url: $('#file_upload').attr('data-url'),
				    type: 'POST',
				    add: function (e, data) {
				        $('#meter_wrapper').find('.meter').addClass('ws_spinner');
				    	$('#meter_wrapper').fadeIn();
				    	$('.is_error').remove();
				        $.ajax({
							url: '/admin/movies/validation/',
							type: 'POST',
							dataType: 'json',
							data: {imdb: $('input[name="imdb"]').val()},
						})
						.done(function(result) {

							if (result.success === false) {
								if (result.message) {
									$('#meter_wrapper').hide();
				        			$('#video_result').html('<p class="is_error">' + result.message + '</p>');
								};
								return false;
							} else {      
								data.formData = {imdb: $('input[name="imdb"]').val()};
								data.submit();
							};

						})
						.fail(function() {
							console.log("error");
						});
				    },
				    done: function(e, data) {
				    	result = data.result;
				        if (result.success) {	

				        	$('input[name="imdb"]').attr('readonly', true);  
				        	$('#meter_wrapper').fadeOut();
				        	$('#meter_wrapper').find('.meter').removeClass('ws_spinner');
				        	$('#upload_result').html(result.field);
		                    $('#video_result').html(result.data.client_name);

		                    var fieldsData = JSON.parse(result.fields);
		                    var $form = $(this).closest('form');
		                    var imageAry = ['poster', 'screenshot'];

		   		            $.each(fieldsData, function(index, val) {
		   
		   		               	var $field = $form.find('[name="'+index+'"]');
		   		               
		   		               	if (typeof(val) == 'object') {

		   		               		if ($.inArray(index, imageAry) !== -1) {

		   		               			if (val.success) {
		   		               				var $imageWrapper = $field.closest('.form-input-box');
		   		               				$field.val(val.files.name);
		   		               				$imageWrapper.find('.fileinput-button').hide();
		   		               				$imageWrapper.find('.upload-success-url').show();
		   		               				$imageWrapper.find('.open-file')
		   		               				.attr({ 'href'  : val.files.url })
		   		               				.addClass('image-thumbnail')
		   		               				.html('<img src="'+val.files.url+'" height="50"/>');
		   		               			}

				   		            } else {

				   		               	var $selectField = $form.find('[name="'+index+'[]"]');
			   		               		var tempAry = [];

			   		               		$.each(val, function(v_index, v_val) {

			   		               			$selectField.find('option').each(function(index, el) {
			   		               				tempAry.push($(this).attr('value'));
			   		               			});

			   		               			$selectField.multiselect( 'destroy' );
			   		               			$selectField.find('option[value="'+v_index+'"]').remove();
			   		               			$selectField.append('<option value="'+v_index+'" selected>'+v_val+'</option>');									
											//$selectField.multiselect();
										});	

				   		            }

		   		              	} else {

		   		               		if ($field.val() == '') {
		   		               			$field.val(val);   		               			
		   		              		}

		   		              	}

  		                    });

				        } else {
				        	$('#meter_wrapper').hide();
				        	$('#video_result').html('<p class="is_error">' + result.message + '</p>');
				        }     
				    },
				    progressall: function (e, data) {
			            var progress = parseInt(data.loaded / data.total * 100, 10);
						$('#progress .progress-bar').html(progress + ' %');
			        },
				    stop: function(e, data) {
						$('#meter_wrapper').fadeOut();

						$('.image-thumbnail').fancybox({
							'transitionIn'	:	'elastic',
							'transitionOut'	:	'elastic',
							'speedIn'		:	600, 
							'speedOut'		:	200, 
							'overlayShow'	:	false
						});	

						deleteImageAnchor();
				    }
				});
				
			};

			file_upload();
		};
	});

</script>
