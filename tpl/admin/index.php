<link type="text/css" rel="stylesheet" href="/assets/grocery_crud/themes/datatables/css/demo_table_jui.css">
<link type="text/css" rel="stylesheet" href="/assets/grocery_crud/themes/datatables/css/datatables.css">
<link type="text/css" rel="stylesheet" href="/assets/grocery_crud/themes/datatables/css/jquery.dataTables.css">
<link type="text/css" rel="stylesheet" href="/assets/grocery_crud/css/ui/simple/jquery-ui-1.10.1.custom.min.css">
<link type="text/css" rel="stylesheet" href="/assets/grocery_crud/themes/datatables/extras/TableTools/media/css/TableTools.css">
<script src="/assets/grocery_crud/js/jquery_plugins/ui/jquery-ui-1.10.3.custom.min.js"></script>
<script src="/assets/grocery_crud/themes/datatables/js/jquery.dataTables.min.js"></script>
<script src="/assets/grocery_crud/themes/datatables/js/datatables-extras.js"></script>
<!-- script src="/assets/grocery_crud/themes/datatables/js/datatables.js"></script -->
<script src="/assets/grocery_crud/themes/datatables/extras/TableTools/media/js/TableTools.min.js"></script>

<script>
	var count1 = '<?=$size_dir;?>';
	var count2 = '<?=byte_format(disk_free_space('/home'));?>';
	var count3 = '<?=byte_format(disk_total_space('/home'));?>';
	var count4 = '<?=$count_dirs;?>';
	var count5 = '<?=$count_films;?>';
</script>

<div class="main_stat_list">

	<div class="stat_item_holder">
		<div class="si_inner">
			<div class="si_name">
				Dirs/Films
			</div>

			<div class="si_budget">
				
				<span id="count4">0</span> / <span id="count5">0</span>
				
				 <!--
				 <span><?=$count_dirs;?></span> / 
				   -->
			</div>
		</div>
	</div>

	<div class="stat_item_holder">
		<div class="si_inner">
			<div class="si_name">
				Size account
			</div> 
			
			<div class="si_budget">
				<span id="count1">0</span> GB
			</div>
		</div>
	</div>

	<div class="stat_item_holder">
		<div class="si_inner">
			<div class="si_name">
				Free space
			</div> 
			
			<div class="si_budget">
				<span id="count2">0</span> GB
			</div>
		</div>
	</div>

	<div class="stat_item_holder">
		<div class="si_inner">
			<div class="si_name">
				Total space 
			</div>

			<div class="si_budget">
				<span id="count3">0</span> GB
			</div>
		</div>
	</div>

</div>