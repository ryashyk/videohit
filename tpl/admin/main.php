<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <title> admin panel </title>
  <link href="/css/admin/reset.css" rel="stylesheet" type="text/css">
  <?php foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
  <?php endforeach; ?>
  <link href="/css/admin/glyphicons.css" rel="stylesheet" type="text/css">
  <link href="/css/admin/font-awesome.css" rel="stylesheet" type="text/css">
  <link href="/css/admin/chosen.css" rel="stylesheet" type="text/css">

  
  <link href="/css/admin/bootstrap-modal.css" rel="stylesheet" type="text/css">
  <link href="/css/admin/bootstrap-modal-bs3patch.css" rel="stylesheet" type="text/css">


  <link href="/css/admin/ion.checkRadio.css" rel="stylesheet" type="text/css">
  <link href="/css/admin/jquery.arcticmodal-0.3.css" rel="stylesheet" type="text/css">
  <link href="/css/admin/select2.min.css" rel="stylesheet" type="text/css">
  <link href="/css/admin/admin.css?t=<?=mt_rand();?>" rel="stylesheet" type="text/css">
  
  <script type="text/javascript" src="/js/admin/jquery-1.11.1.min.js"></script>

  <?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
  <?php endforeach; ?>

  <script type="text/javascript" src="/js/admin/jquery.easing.1.3.js"></script>
  <script type="text/javascript" src="/js/admin/chosen.jquery.min.js"></script>
  <script type="text/javascript" src="/js/admin/select2.min.js"></script>
  <script type="text/javascript" src="/js/admin/countUp.js"></script>

  <script type="text/javascript" src="/js/admin/bootstrap-modal.js"></script>
  <script type="text/javascript" src="/js/admin/bootstrap-modalmanager.js"></script>

  <script type="text/javascript" src="/js/uppod-0.5.34.js"></script>
  <script type="text/javascript" src="/styles/video206-1302.js"></script>
  <script type="text/javascript" src="/js/swfobject.js"></script>
  <script type="text/javascript" src="/js/uppod_api.js"></script>
  <script type="text/javascript" src="/player/CodoPlayer.js" ></script>
  <script type="text/javascript" src="/js/flw/swfobject.js"></script>

  <script type="text/javascript" src="/js/admin/ion.checkRadio.min.js"></script>
  <script type="text/javascript" src="/js/admin/jquery.arcticmodal-0.3.min.js"></script>
  <script type="text/javascript" src="/js/admin/js.js"></script>
 
  <script type="text/javascript" src="/js/admin/admin_script.js?t=<?=rand();?>"></script>

  <!--[if IE]>
    <link rel="stylesheet" type="text/css"  href="/css/admin/style_ie.css" />
    <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>

<body>
  
  <div style="display:none;">
    <div id="responsive" class="modal hide fade" tabindex="-1" data-width="760">
      <div class="modal-header">

        <a type="button" class="close modal-close" data-dismiss="modal" aria-hidden="true">×</a>
        <h4 id="modal-title" class="modal-title"></h4>
                  <span id="modal-url"></span>
      </div>
      <div class="modal-body">
        <div id="video" class="video-blk"></div>
        <div id="mediaspace"></div>
      </div>
      <div class="modal-footer">
        <button type="button" data-dismiss="modal" class="btn btn-sample">Close</button>
      </div>
    </div>
  </div>





  <!-- header -->
  <header id="header">
    <div class="logo">
      <a href="<?=site_url('admin');?>">
        <h1>admin panel</h1>
      </a>
    </div>
    <div class="btn_group pull-right">
      <div class="login_nfo">
          <img src="<?=image_thumb('upls/users/'.$user->img,50,50);?>" width="30" height="30">
          <span> <?=$user->name;?></span>
      </div>
      <a href="<?=site_url('admin/auth/logout')?>" class="btn">
        <span class="glyphicon glyphicon-log-out"></span>
      </a>
    </div>
     <nav class="navbar_menu">
       <?=$menu;?>
     </nav>
  </header>
  <!-- end-of-header -->

  <!-- content -->
  <main id="main">

    <div class="container">
      <div class="brd_crb">
        <ul>
          <?php foreach($breadcrumbs as $k=>$v):?>
           <li><a href="<?=site_url($k);?>"><?=$v;?></a></li>
          <?php endforeach;?>
        </ul>
      </div>
      <div class="content">
        <div class="widjet_item">
          <header>
             <h1><?=$current_section;?></h1>
          </header>
          <div class="inner_body">
              <?php echo $output; ?>
          </div>
        </div>
      </div>

    </div>

    <!-- footer -->
    <footer id="footer">
      <div id="copiright_web">

      </div>
    </footer>
    <!-- end-of-footer -->
  </main>
  <!-- end-of-content -->
</body>

</html>

















