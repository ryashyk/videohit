  <main id="main">

  

    <div class="container">

      <div class="brd_crb">

        <ul>

          <?php foreach($breadcrumbs as $k=>$v):?>

           <li><a href="<?=site_url($k);?>"><?=$v;?></a></li>
          <?php endforeach;?>

        </ul>

      </div>

      <div class="content">

        <div class="widjet_item">

          <header>

             <h1><?=$current_section;?></h1>

          </header>

          <div class="inner_body">

              <?php echo $output; ?>

          </div>

        </div>

      </div>

      

    </div>

    </main>