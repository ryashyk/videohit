<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="cache-control" content="max-age=0" />
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
	<meta http-equiv="pragma" content="no-cache" />
    <title>Best Fails Videos 2015</title>
    <link href="/css/bootstrap.min.css?t=2069571659" rel="stylesheet" type="text/css">
    <link href="/css/style.css?t=160929740" rel="stylesheet" type="text/css">
    <!--[if lte IE 9]>
	    <script type="text/javascript" src="/js/html5shiv.min.js"></script>
	    <script type="text/javascript" src="/js/respond.min.js"></script>
	    <link rel="stylesheet" type="text/css"  href="/css/style_ie.css" /> 
	<![endif]-->
</head>

<body>

	<!-- header -->
	<header id="header">
		<div class="container">
			<div class="row">
				
				<nav class="navbar navbar-inverse" role="navigation">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<a class="navbar-brand" href="http://videoboxer.co/">Videoboxer</a>
					</div>
					<ul class="nav navbar-nav navbar-right">
						<li><a data-toggle="modal" href='#modal-login'>Login</a></li>	
						<li><a data-toggle="modal" href='#modal-register'>Sign Up</a></li>								
					</ul>
				</nav>
			</div>
		</div>
	</header>
	<!-- .header -->

	<div class="modal fade" id="modal-login">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="loginform" class="form-horizontal" role="form">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Sign In</h4>
					</div>

					<div class="modal-body">

		                <div style="margin-bottom: 15px" class="input-group">
		                    <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
		                    <input id="login-username" type="text" class="form-control" name="username" value="" placeholder="Email">
		                </div>
       

		                <div style="margin-bottom: 15px" class="input-group">
		                    <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
		                    <input id="login-password" type="password" class="form-control" name="password" placeholder="Password">
		                </div>
		                               
		                <div class="input-group">
		                    <div class="checkbox">
		                        <label>
		                	        <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
		                        </label>
		                    </div>
		                    <div class="fgt_psw"><a href="#">Forgot password?</a></div>
						</div>

					</div>

					<div class="modal-footer">
						<button id="btn-login" type="submit" class="btn btn-success btn-ebmed">Login  </button>
					</div>

				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal-register">
		<div class="modal-dialog">
			<div class="modal-content">
				<form id="signupform" class="form-horizontal" role="form">

					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-title">Create an account</h4>
					</div>
					
					<div class="modal-body">
						
		               	<div class="form-group">
                            <label for="email" class="col-md-4 control-label">Email address</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="email" placeholder="Email Address">
                            </div>
                        </div>
                                    
                        <div class="form-group">
                            <label for="firstname" class="col-md-4 control-label">Password</label>
                            <div class="col-md-8">
                                <input type="password" class="form-control" name="psw" placeholder="Password">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="lastname" class="col-md-4 control-label">Confirm Password</label>
                            <div class="col-md-8">
                                <input type="password" class="form-control" name="reppsw" placeholder="Confirm Password">
                            </div>
                        </div>                  

					</div>

					<div class="modal-footer">
						<button id="btn-fbsignup" type="submit" class="btn btn-primary btn-ebmed"> Sign Up </button>
					</div>

				</form>
			</div>
		</div>
	</div>
	
	<script type="text/javascript" src="/js/uppod_api.js"></script>
    <script type="text/javascript" src="/js/uppod-0.5.34.js"></script>
	<script type="text/javascript" src="/styles/video206-1287.js?t=808308908"></script>
	<script type="text/javascript" src="/js/swfobject.js"></script>

	<!-- content -->
	<div id="main">
		<div class="container">
			<div class="row">
				<div class="video_box_holder">

					<h1>Best Fails Videos 2015 </h1>
					<div class="video_inner">
						<div class="modal fade" id="modal-embed" style="display:none;">

							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>			
									</div>
									<div class="modal-body">
										<p>
											Hint: you might customize width and height to fit your needs
										</p>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>

						</div>
						<div class="video_box">
							<div id="videoplayer1" class="video_wrp"></div>
							<span class="play_video" id="play_video"></span>
						</div>

					</div>
	
                    <script type="text/javascript">

						var test_video = document.createElement("video");
						var mediasupport = { video: (test_video.play)? true : false}
						var btn_play = document.getElementById('play_video');

						function uppodEvent(playerID,event) { 
							switch(event){		
								case 'init': 	
									console.log('init');		
									break;				
								case 'start': 
									console.log('start');					
									break;			
								case 'play': 
									console.log('play');	
									popunder();				
									break;				
								case 'pause': 
									console.log('pause');	
									btn_play.style.display = 'block';				
									break;				
								case 'stop': 
									console.log('stop');					
									break;				
								case 'seek': 
									console.log('seek');
									break;				
								case 'loaded':	
									console.log('loaded');			
									break;				
								case 'end':	
									console.log('end');				
									break;				
								case 'download':
									console.log('download');						
									break;				
								case 'quality':	
									console.log('quality');			
									break;			
								case 'error':
									console.log('error');					
									break;					
								case 'ad_end':	
									console.log('ad_end');			
									break;				
								case 'pl':
									console.log('pl');				
									break;	
								case 'volume':
									console.log('volume');			
									break;
							}
						};
						
						// Commands
						function uppodSend(playerID,com,callback) {
							document.getElementById(playerID).sendToUppod(com);
						};
						
						// Requests
						function uppodGet(playerID,com,callback) {
							return document.getElementById(playerID).getUppod(com);
						};

						function cBrowser() {
						  	var ua = navigator.userAgent;

						  	var bName = function () {
						   		if (ua.search(/MSIE/) > -1) return "ie";
						   		if (ua.search(/Firefox/) > -1) return "firefox";
						   		if (ua.search(/Opera/) > -1) return "opera";
						   		if (ua.search(/Chrome/) > -1) return "chrome";
						   		if (ua.search(/Safari/) > -1) return "safari";
						   		if (ua.search(/Konqueror/) > -1) return "konqueror";
						   		if (ua.search(/Iceweasel/) > -1) return "iceweasel";
						   		if (ua.search(/SeaMonkey/) > -1) return "seamonkey";
						   	}();

						  	var version = function (bName) {
						   		switch (bName) {
						    		case "ie" : return (ua.split("MSIE ")[1]).split(";")[0];break;
						   	 		case "firefox" : return ua.split("Firefox/")[1];break;
						    		case "opera" : return ua.split("Version/")[1];break;
						    		case "chrome" : return (ua.split("Chrome/")[1]).split(" ")[0];break;
						    		case "safari" : return (ua.split("Version/")[1]).split(" ")[0];break;
						    		case "konqueror" : return (ua.split("KHTML/")[1]).split(" ")[0];break;
						    		case "iceweasel" : return (ua.split("Iceweasel/")[1]).split(" ")[0];break;
						    		case "seamonkey" : return ua.split("SeaMonkey/")[1];break;
						   		}
						   	}(bName);
						   	// [bName,bName + version.split(".")[0],bName + version]
						   	
						   	if((typeof version) != 'undefined'){
								return [bName , version.split(".")[0]];
							}else{
								return ['', 0];
							}
						};

						var flash = false,
							browser = cBrowser();

						switch (browser[0]) {
							case 'chrome':
								if (browser[1] < 42 || !mediasupport.video) {
									flash = true;
								};
							break;
							case 'opera':
								if (browser[1] < 34 || !mediasupport.video) {
									flash = true;
								};
							break;
							case 'firefox':
								if (browser[1] < 42 || !mediasupport.video) {
									flash = true;
								};
							break;
							case 'safari':
								if (browser[1] < 9 || !mediasupport.video) {
									flash = true;
								};
							break;
							case 'ie':
								if (browser[1] < 9 || !mediasupport.video) {
									flash = true;
								};
							break;

						};

						if (!flash) {

							var position = null;

							var uppod = new Uppod({
							   	m:"video",
								uid:"videoplayer1",
								file:"<?=$file;?>" , 
								st:"uppodvideo",
								poster:"http://videoboxer.co/upls/files/9e8d1e5c/thumb/best-fails-videos-2015-1-0-800-480.jpeg" ,
								width : 800,
								height : 480,
							});
							
							btn_play.onclick = function() {
								if (position != null) {
									uppod.Play();
									console.log(position + " onplay");
								} else {
									uppod.Play();
								};
								btn_play.style.display = 'none';
							};			

							document.getElementById("videoplayer1").addEventListener("pause",function() {
								btn_play.style.display = 'block';
								position = uppod.CurrentTime();
								uppod.Stop();
								console.log(position + " onstop");

							},false);
						

						} else {

							var flashvars1 = {
								"st"	: "/styles/video206-1287.txt",
								"uid"	: "videoplayer1",
								"debug"	: 1,
								"file" 	: "<?=urlencode($file);?>",
								'poster': "http://videoboxer.co/upls/files/9e8d1e5c/thumb/best-fails-videos-2015-1-0-800-480.jpeg"
							}; 

							var params1 = {
								bgcolor:"#ffffff", 
								wmode:"transparent", 
								width : 800,
								height: 480,
								allowFullScreen:"true", 
								allowScriptAccess:"always"
							}; 

							swfobject.embedSWF("/player/uppod.swf", "videoplayer1", "800", "480", "10.0.0.0", false, flashvars1, params1);

						};

					</script>
				</div>
			</div>
		</div>
	</div>
	<!-- .content -->

	<!-- footer -->

	<footer id="footer">
		<nav class="navbar navbar-inverse navbar-fixed-bottom" role="navigation">
			<div class="container">
				<!-- Collect the nav  links, forms, and other content for toggling -->
				<div class="navbar-collapse ">
					<ul class="nav navbar-nav navbar-right">
					    <li><a href="http://videoboxer.co/page/terms-of-service">Terms of Service</a></li>
                        <li><a href="http://videoboxer.co/page/dmca">DMCA</a></li>
                        <li><a href="http://videoboxer.co/page/contact-us">Contact us</a></li>
                        <li><a href="http://videoboxer.co/page/faq">FAQ</a></li>
				 	</ul>
				</div><!-- /.navbar-collapse -->
			</div>
		</nav>	

	</footer>
	<!-- .footer -->

	<!-- scripts -->
	<script type="text/javascript" src="/js/jquery-2.1.4.min.js"></script> 
	<script type="text/javascript" src="/js/bootstrap.min.js"></script> 
	<script type="text/javascript" src="/js/jquery.validate.min.js"></script> 
	<script type="text/javascript" src="/js/script.js?t=2141613478"></script> 
	<!-- .scripts -->

</body>
</html>